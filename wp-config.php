<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'fujiservices_stg' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '#F>On{C4CGWtG+^FjPXlyQ}e:n4{]w68d-03*jrXb;Gp#H(/LcPMSTX8$L G%:A&' );
define( 'SECURE_AUTH_KEY',  'Vu$6v7>*T9V.GH*hd/s<$b+cR.:}@^4L-N 2MWm$vH?M-a+tklw-E@DpnJ>d9<C^' );
define( 'LOGGED_IN_KEY',    'ZY)9RGtf?.%|+vF!bG3oQcf^M4n<[Hn(B:8R<1mYFUHcL6#G1nysdU;f6c}c];08' );
define( 'NONCE_KEY',        '!XM4(b@BL#G|YFw2,02+XekW=]gad~WI;yW2(Rs8#]k?eD)$+$Kc1&]e@=2tl+!s' );
define( 'AUTH_SALT',        '-SyjbtG/1sB6!ev9~0M~9V54M`q-<H5w~fcc[W36Wp3R>stJNMSIr?K&]l:I0C;K' );
define( 'SECURE_AUTH_SALT', 's/Lz{)Sj9*F-RcagO)LViSP/`8uhXLmyc%>ULH;MKl.G7_5X`c7xPJ$&Q=w&yyRK' );
define( 'LOGGED_IN_SALT',   '=V*/c/GA%*Q8Vf~3u04B_=<yyVZYQpCaI4/_YeHul|OEHOqjD[TL9054Xk2K41B[' );
define( 'NONCE_SALT',       'SYQcx~|7r5Dd,GsHAL-.=-x0b_ML<q`J]N6y>*$X!yLQZJ6X>GX5H@5_/{-q(bzS' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
//define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */


//デバッグモード設定
define('WP_DEBUG', true);
//define('WP_DEBUG', true); // デバッグモードを有効化
if (WP_DEBUG) { // デバッグモードの時だけ
        define('WP_DEBUG_LOG', true); // debug.log ファイルに記録
        define('WP_DEBUG_DISPLAY', true); // ブラウザ上に表示
}else{
        define('WP_DEBUG_LOG', false); // debug.log ファイルに記録しない
        define('WP_DEBUG_DISPLAY', false); // ブラウザ上に表示しない
        //@ini_set('display_errors',0); // ブラウザ上に表示しない
}

define( 'WP_MAX_MEMORY_LIMIT', '256M' );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';

