# *** Additional script code area ***
# Allow root or not
ALLOW_ROOT=""
# Set alias for WP-CLI script
shopt -s expand_aliases
alias php='/usr/bin/php'
alias wp='/usr/bin/php /var/www/html/wp-content/wp-cli.phar'
