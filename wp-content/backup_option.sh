###########################################
# WP-OPTIONからデータを取得する
###########################################
ALLOW_ROOT=$1

##ACTIONMODEの設定値により必要なオプション情報をDBより取得する
#actmode=`wp option get backup_actionmode  ${ALLOW_ROOT}`    #アクションモード 1:BASIC 2:ADVANCED
#if [ $actmode -ne 1 ]; then
#  echo "2:ADVANCED"
#else
#  echo "1:BASIC"
#fi

#if [ $actmode -ne 1 ]; then
#backup_srchomepath=`wp option get backup_srchomepath ${ALLOW_ROOT}`    #転送元（バックアップ）データパス
#echo $backup_srchomepath
#backup_desthomepath=`wp option get backup_desthomepath ${ALLOW_ROOT}`    #転送先（復元）データパス
#echo $backup_desthomepath
#fi

backup_srcdirname=`wp option get backup_srcdirname ${ALLOW_ROOT}`    #転送元（バックアップ）ディレクトリ名
echo $backup_srcdirname
backup_destdirname=`wp option get backup_destdirname ${ALLOW_ROOT}`    #転送先（復元）ディレクトリ名
echo $backup_destdirname
backup_tempdirname=`wp option get backup_tempdirname ${ALLOW_ROOT}`    #一時ディレクトリ名
echo $backup_tempdirname

backup_fnamemysqldata=`wp option get backup_fnamemysqldata ${ALLOW_ROOT}`    #データベース圧縮ファイル名
echo $backup_fnamemysqldata
backup_fnamewpthemes=`wp option get backup_fnamewpthemes ${ALLOW_ROOT}`    #WPテーマ圧縮ファイル名
echo $backup_fnamewpthemes
backup_fnamewpplugins=`wp option get backup_fnamewpplugins ${ALLOW_ROOT}`    #WPプラグイン圧縮ファイル名
echo $backup_fnamewpplugins
backup_fnamewpuploads=`wp option get backup_fnamewpuploads ${ALLOW_ROOT}`    #WPアップロード圧縮ファイル名
echo $backup_fnamewpuploads
backup_fnamewpconfig=`wp option get backup_fnamewpconfig ${ALLOW_ROOT}`    #WP-CONFIGファイル名
echo $backup_fnamewpconfig
backup_fnameinfodata=`wp option get backup_fnameinfodata ${ALLOW_ROOT}`    #バックアップ情報ファイル名
echo $backup_fnameinfodata

#if [ $actmode -ne 1 ]; then
#backup_s3backup=`wp option get backup_s3backup ${ALLOW_ROOT}`    #s3ストレージへのバックアップ 1する,0しない
#echo $backup_s3backup
#backup_s3accesskey=`wp option get backup_s3accesskey ${ALLOW_ROOT}`    #s3ストレージ アクセスキー
#echo $backup_s3accesskey
#backup_s3secretkey=`wp option get backup_s3secretkey ${ALLOW_ROOT}`    #s3ストレージ シークレットキー
#echo $backup_s3secretkey
#backup_s3endpoint=`wp option get backup_s3endpoint ${ALLOW_ROOT}`    #s3ストレージ エンドポイントURL
#echo $backup_s3endpoint
#backup_s3backetname=`wp option get backup_s3backetname ${ALLOW_ROOT}`    #s3ストレージ エンドポイントURL
#echo $backup_s3backetname
#
#backup_sshsrchostname=`wp option get backup_sshsrchostname ${ALLOW_ROOT}`    #ssh転送元ホスト名
#echo $backup_sshsrchostname
#backup_sshsrcportno=`wp option get backup_sshsrcportno ${ALLOW_ROOT}`    #ssh転送元ポートNo
#echo $backup_sshsrcportno
#backup_sshsrcusername=`wp option get backup_sshsrcusername ${ALLOW_ROOT}`    #ssh転送元ユーザ名
#echo $backup_sshsrcusername
#backup_sshsrcprivatekeypath=`wp option get backup_sshsrcprivatekeypath ${ALLOW_ROOT}`    #ssh転送元プライベートキーパス
#echo $backup_sshsrcprivatekeypath
#backup_sshsrcprivatekeyname=`wp option get backup_sshsrcprivatekeyname ${ALLOW_ROOT}`    #ssh転送元プライベートキー名称
#echo $backup_sshsrcprivatekeyname
#backup_sshnotification=`wp option get backup_sshnotification ${ALLOW_ROOT}`    #ssh経由バックアップ完了通知 1する,0しない
#echo $backup_sshnotification
#
#backup_sshdesthostname=`wp option get backup_sshdesthostname ${ALLOW_ROOT}`    #ssh転送先ホスト名
#echo $backup_sshdesthostname
#backup_sshdestportno=`wp option get backup_sshdestportno ${ALLOW_ROOT}`    #ssh転送先ポートNo
#echo $backup_sshdestportno
#backup_sshdestusername=`wp option get backup_sshdestusername ${ALLOW_ROOT}`    #ssh転先ユーザ名
#echo $backup_sshdestusername
#backup_sshdestprivatekeypath=`wp option get backup_sshdestprivatekeypath ${ALLOW_ROOT}`    #ssh転送先プライベートキーパス
#echo $backup_sshdestprivatekeypath
#backup_sshdestprivatekeyname=`wp option get backup_sshdestprivatekeyname ${ALLOW_ROOT}`    #ssh転送先プライベートキー名称
#echo $backup_sshdestprivatekeyname
#fi


#backup_newsiteurl=`wp option get backup_newsiteurl ${ALLOW_ROOT}`    #移行先の新しいURL（復元時に置き換える）
backup_newsiteurl=`wp option get siteurl ${ALLOW_ROOT}`    #移行先の「サイトURL」に置き換える（復元時に置き換える）
echo $backup_newsiteurl

admin_email=`wp option get admin_email ${ALLOW_ROOT}`    #管理者用メールアドレスを取得する
echo $admin_email



###########################################
# WP-OPTIONから取得したデータから
# 必要な変数を設定する
###########################################

#バックアップデータベースファイル名（日付あり）
backup_fnamemysqldata1=${backup_fnamemysqldata//\$\{SYSDATE\}/${SYSDATE}}
#バックアップデータベースファイル名（日付なし）
backup_fnamemysqldata2=${backup_fnamemysqldata//\$\{SYSDATE\}/}

#バックアップＷＰテーマファイル名（日付あり）
backup_fnamewpthemes1=${backup_fnamewpthemes//\$\{SYSDATE\}/${SYSDATE}}
#バックアップＷＰテーマファイル名（日付なし）
backup_fnamewpthemes2=${backup_fnamewpthemes//\$\{SYSDATE\}/}

#バックアップＷＰプラグインファイル名（日付あり）
backup_fnamewpplugins1=${backup_fnamewpplugins//\$\{SYSDATE\}/${SYSDATE}}
#バックアップＷＰプラグインファイル名（日付なし）
backup_fnamewpplugins2=${backup_fnamewpplugins//\$\{SYSDATE\}/}

#バックアップＷＰアップロードファイル名（日付あり）
backup_fnamewpuploads1=${backup_fnamewpuploads//\$\{SYSDATE\}/${SYSDATE}}
#バックアップＷＰアップロードファイル名（日付なし）
backup_fnamewpuploads2=${backup_fnamewpuploads//\$\{SYSDATE\}/}

#バックアップＷＰアップロードファイル名（日付あり）
backup_fnamewpconfig1=${backup_fnamewpconfig//\$\{SYSDATE\}/${SYSDATE}}
#バックアップＷＰアップロードファイル名（日付なし）
backup_fnamewpconfig2=${backup_fnamewpconfig//\$\{SYSDATE\}/}

#バックアップ情報ファイル名（日付あり）
backup_fnameinfodata1=${backup_fnameinfodata//\$\{SYSDATE\}/${SYSDATE}}
#バックアップ情報ファイル名（日付なし）
backup_fnameinfodata2=${backup_fnameinfodata//\$\{SYSDATE\}/}
#バックアップ情報ファイル名（一時ファイル）
backup_fnameinfodata_temp=temp_${backup_fnameinfodata//\$\{SYSDATE\}/}

#ABSPATHのディレクトリパスを取得する
ABSPATH=`wp eval "echo ABSPATH;" ${ALLOW_ROOT}`

#転送元バックアップディレクトリ
#DBパラメータは使用しない（ABSPATH使用）
#SRC_BACKUP_DIR=${backup_srchomepath}${backup_srcdirname}/
SRC_BACKUP_DIR=${ABSPATH}${backup_srcdirname}/
#転送先バックアップディレクトリ
#DBパラメータは使用しない（ABSPATH使用）
#DEST_BACKUP_DIR=${backup_desthomepath}${backup_destdirname}/
DEST_BACKUP_DIR=${ABSPATH}${backup_destdirname}/


#カレントディレクトリ取得
# $0 で実行コマンドを取得
# dirname でベースディレクトリを取得
# サブシェル内で cd で移動
# pwd で絶対パスの取得

CURRENT_BACKUP_DIR=$(cd $(dirname $0); pwd)/
