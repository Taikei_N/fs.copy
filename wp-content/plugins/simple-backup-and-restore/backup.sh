#!/bin/bash

# backup.sh
#
# -----------------------------------------------------------------------------
# Title : バックアップ処理
# -----------------------------------------------------------------------------
# 2018.10.13 created
#
# Description :
#   バックアップする
#
# Arguments :
#   [Arg1] WP_CONTENT_DIR
#
# Return value :
#   =0: OK
#   >0: ERROR CODE
#
#
# -----------------------------------------------------------------------------

#################################################
#バックアップフォルダをクリアする
#################################################
function removeoldfile() {
	#バックアップフォルダをクリアする
	###########source ${WP_CONTENT_DIR}delete_backup_files.sh ${PROVISION} ${DBNAME}
	if [ -e ${SRC_BACKUP_DIR}${backup_fnamewpthemes2} ]; then
	    rm -f ${SRC_BACKUP_DIR}${backup_fnamewpthemes2}
	    echo "Remeved ${backup_fnamewpthemes}"
	fi
	if [ -e ${SRC_BACKUP_DIR}${backup_fnamewpplugins2} ]; then
	    rm -f ${SRC_BACKUP_DIR}${backup_fnamewpplugins2}
	    echo "Remeved ${backup_fnamewpplugins}"
	fi
	if [ -e ${SRC_BACKUP_DIR}${backup_fnamewpuploads2} ]; then
	    rm -f ${SRC_BACKUP_DIR}${backup_fnamewpuploads2}
	    echo "Remeved ${backup_fnamewpuploads}"
	fi
	if [ -e ${SRC_BACKUP_DIR}${backup_fnamemysqldata2} ]; then
	    rm -f ${SRC_BACKUP_DIR}${backup_fnamemysqldata2}
	    echo "Remeved ${backup_fnamemysqldata}"
	fi
}

#################################################
# バックアップ情報ファイルを作成する
#################################################
function makeInfoFile() {
	#バックアップ情報ファイルを作成する
	###########source ${WP_CONTENT_DIR}delete_backup_files.sh ${PROVISION} ${DBNAME}
	if [ -e ${SRC_BACKUP_DIR}${backup_fnameinfodata_temp} ]; then
	    mv ${SRC_BACKUP_DIR}${backup_fnameinfodata_temp} ${SRC_BACKUP_DIR}${backup_fnameinfodata1}
    else
        if [ $NO_OF_ARGS -eq 1 ]; then
            echo "${SYSDATE} Manual backup executed." > ${SRC_BACKUP_DIR}${backup_fnameinfodata1}
        else
            echo "${SYSDATE} Auto backup executed." > ${SRC_BACKUP_DIR}${backup_fnameinfodata1}
        fi
	fi
	echo "${home}; ${tableprefix}" >> ${SRC_BACKUP_DIR}${backup_fnameinfodata1}

}


#################################################
#データベースのバックアップを行う（WP-CLIに変更）
#################################################
function backupdatabase() {
	#最終バックアップにファイル名を設定する（バックアップする直前に最終バックアップ情報を更新する）
	wp option update backup_last_backup_file ${backup_fnamemysqldata1} ${ALLOW_ROOT}
	#データベースのバックアップを行う（WP-CLIに変更）
	echo データベースのバックアップを行う
	echo "wp db export ${SRC_BACKUP_DIR}${backup_fnamemysqldata1} ${ALLOW_ROOT}"
	wp db export ${SRC_BACKUP_DIR}${backup_fnamemysqldata1} ${ALLOW_ROOT}
#########${HOMEDIR}backup_database.sh  ${PROVISION} ${DBNAME} ${SYSDATE}
}


#################################################
#ファイルのバックアップを行う
#################################################
function backupwpfiles() {
	#ファイルのバックアップを行う
	#########source ${WP_CONTENT_DIR}backup_wp-contents.sh
	tar -zcvf ${SRC_BACKUP_DIR}${backup_fnamewpthemes1} -C ${WP_CONTENT_DIR} themes
	tar -zcvf ${SRC_BACKUP_DIR}${backup_fnamewpplugins1} -C ${WP_CONTENT_DIR} plugins
	tar -zcvf ${SRC_BACKUP_DIR}${backup_fnamewpuploads1} -C ${WP_CONTENT_DIR} uploads
	pushd ${ABSPATH}
if [ -e ./wp-config.php ]; then
	\cp -f ./wp-config.php ${SRC_BACKUP_DIR}${backup_fnamewpconfig1}
fi
if [ -e ../wp-config.php ]; then
	\cp -f ../wp-config.php ${SRC_BACKUP_DIR}${backup_fnamewpconfig1}
fi
	popd
}


#################################################
#バックアップファイルをオブジェクトストレージに転送する
#################################################
function backups3strage() {
	#バックアップファイルをオブジェクトストレージに転送する
	#########source ${WP_CONTENT_DIR}send_to_object_strage.sh 
	s3cmd put ${SRC_BACKUP_DIR}${backup_fnamemysqldata1} s3://${backup_s3backetname}
	s3cmd put ${SRC_BACKUP_DIR}${backup_fnamewpthemes1}  s3://${backup_s3backetname}
	s3cmd put ${SRC_BACKUP_DIR}${backup_fnamewpplugins1}  s3://${backup_s3backetname}
	s3cmd put ${SRC_BACKUP_DIR}${backup_fnamewpuploads1}  s3://${backup_s3backetname}
}


#################################################
#ファイル名を日付なしに変更する
#################################################
function renamebackupfiles() {
	#ファイル名を日付なしに変更する
	cp ${SRC_BACKUP_DIR}${backup_fnamewpthemes1}  ${SRC_BACKUP_DIR}WP_THEMES.tar.gz
	cp ${SRC_BACKUP_DIR}${backup_fnamewpplugins1} ${SRC_BACKUP_DIR}WP_PLUGINS.tar.gz
	cp ${SRC_BACKUP_DIR}${backup_fnamewpuploads1} ${SRC_BACKUP_DIR}WP_UPLOADS.tar.gz
	cp ${SRC_BACKUP_DIR}${backup_fnamemysqldata1} ${SRC_BACKUP_DIR}MYSQL_DATABASE.sql
	cp ${SRC_BACKUP_DIR}${backup_fnamewpconfig1} ${SRC_BACKUP_DIR}wp-config.php
	cp ${SRC_BACKUP_DIR}${backup_fnamewpconfig1} ${SRC_BACKUP_DIR}wp-config.txt
	cp ${SRC_BACKUP_DIR}${backup_fnameinfodata1} ${SRC_BACKUP_DIR}BACKUP_INFODATA.txt
}





#################################################
# バックアップシェルスクリプト
#
#
#################################################

#引数の個数を変数に設定する。
NO_OF_ARGS=$#

# 実行時に指定された引数の数が 1個または2個でなければエラー終了。
if [ $NO_OF_ARGS -eq 0 ] || [ $NO_OF_ARGS -gt 2 ]; then
   echo "第１引数には必ずWP-CONTENTディレクトリを設定してください。"
   echo "第２引数にCOMPLETE.TXT出力有無を指定します※オプショナル（デフォルトで出力あり）"
   exit 1
fi


####################################################
# 変数の設定
####################################################
#カレントディレクトリをWP-CONTENTに移動する
cd $1
#WP-CONTENTパスを取得する
WP_CONTENT_DIR=$1/

#logファイルを作成する
LOG_FILE="backup-process-status.txt"
COMPLETE_FILE="complete.txt"
###WP_CONTENT_DIR=`wp eval "echo WP_CONTENT_DIR;" ${ALLOW_ROOT}`
CURRENT_BACKUP_DIR=${WP_CONTENT_DIR}


#ROOT権限デフォルト値設定
ALLOW_ROOT=""
#WP設定画面で設定したスクリプトを読み込む
source ${WP_CONTENT_DIR}backup_addtional_script.sh


#ステータスファイルを生成する
rm -f ${CURRENT_BACKUP_DIR}${LOG_FILE}
touch ${CURRENT_BACKUP_DIR}${LOG_FILE}
chown apache:apache ${CURRENT_BACKUP_DIR}${LOG_FILE}
chmod 777 ${CURRENT_BACKUP_DIR}${LOG_FILE}
#ステータスファイルに出力する（第二引数設定時は作成しない）
if [ $NO_OF_ARGS -eq 1 ]; then
    #echo `date '+%Y%m%d_%H%M%S'`: ROOT権限 = ${ALLOW_ROOT} >> ${CURRENT_BACKUP_DIR}${LOG_FILE}
    echo `date '+%Y%m%d_%H%M%S'`: バックアップ処理を開始しました >> ${CURRENT_BACKUP_DIR}${LOG_FILE}
fi


#タイムスタンプを取得、変数に設定する
SYSDATE=`date '+%Y%m%d_%H%M%S_'`

#完了ファイルを生成する（第二引数設定時は作成しない）
if [ $NO_OF_ARGS -eq 1 ]; then
    echo `date '+%Y%m%d_%H%M%S'`: "オプションデータの読み込みを開始しました"  >> ${CURRENT_BACKUP_DIR}${LOG_FILE}
fi
#WP設定画面で設定した情報を変数に取得する
source ${WP_CONTENT_DIR}backup_option.sh "${ALLOW_ROOT}"

#WP-CLIが正常に動作しているかどうか確認する
#echo `date '+%Y%m%d_%H%M%S'`": エラー判定 admin_email：　${admin_email} ${#admin_email}"
if [ ${#admin_email} -eq 0 ]; then
#logファイルを作成する
echo `date '+%Y%m%d_%H%M%S'`": バックアップ処理を中断しました" > ${WP_CONTENT_DIR}${LOG_FILE}
echo "スクリプトが実行できませんでした<br />バックアップ処理を中断しました" > ${WP_CONTENT_DIR}${COMPLETE_FILE}

echo リストア処理を中断しました
exit 1
fi

home=`wp option get home ${ALLOW_ROOT}`    #HOME URL
echo $home
tableprefix=`wp option get BACKUP_TABLEPREFIX ${ALLOW_ROOT}`    #TABLE PREFIX
echo $tableprefix

#完了ファイルを生成する（第二引数設定時は作成しない）
if [ $NO_OF_ARGS -eq 1 ]; then
    echo `date '+%Y%m%d_%H%M%S'`: "オプションデータの読み込みを完了しました"  >> ${CURRENT_BACKUP_DIR}${LOG_FILE}
fi


####################################################
# バックアップ初期処理
####################################################

#バックアップフォルダをクリアする
echo バックアップファイルをクリアする
removeoldfile

#バックアップ情報ファイルを作成する
echo バックアップ情報ファイル${backup_fnameinfodata1}を作成する
makeInfoFile


####################################################
#データベースのバックアップを行う（WP-CLIに変更）
####################################################

echo データベースのバックアップを行う
#backupdatabase
#ここに展開する
        #最終バックアップにファイル名を設定する（バックアップする直前に最終バッ>クアップ情報を更新する）
        wp option update backup_last_backup_file ${backup_fnamemysqldata1} ${ALLOW_ROOT}
        #データベースのバックアップを行う（WP-CLIに変更）
        echo データベースのバックアップを行う
        echo "wp db export ${SRC_BACKUP_DIR}${backup_fnamemysqldata1} ${ALLOW_ROOT}"
        wp db export ${SRC_BACKUP_DIR}${backup_fnamemysqldata1} ${ALLOW_ROOT}
#########${HOMEDIR}backup_database.sh  ${PROVISION} ${DBNAME} ${SYSDATE}


#完了ファイルを生成する（第二引数設定時は作成しない）
if [ $NO_OF_ARGS -eq 1 ]; then
    echo `date '+%Y%m%d_%H%M%S'`: データベースのバックアップを完了しました >> ${CURRENT_BACKUP_DIR}${LOG_FILE}
fi


####################################################
# ファイルのバックアップ処理
####################################################

#ファイルのバックアップを行う
echo ファイルのバックアップを行う
backupwpfiles
#完了ファイルを生成する（第二引数設定時は作成しない）
if [ $NO_OF_ARGS -eq 1 ]; then
    echo `date '+%Y%m%d_%H%M%S'`: ファイルのバックアップを完了しました >> ${CURRENT_BACKUP_DIR}${LOG_FILE}
fi


echo バックアップファイルをオブジェクトストレージに転送する
#バックアップファイルをオブジェクトストレージに転送する
#※設定情報をみて実行するようにすること※要コメントアウトを外すこと
#backups3strage


#バックアップ情報ファイルに結果を出力する
#この処理は次のrenamebackupfiles関数の必ず直線に行うこと
echo "Complete" >> ${SRC_BACKUP_DIR}${backup_fnameinfodata1}


####################################################
# 日付なしバックアップファイルを生成
####################################################

echo ファイル名を日付なしに変更する
#ファイル名を日付なしに変更する
renamebackupfiles


####################################################
# バックアップ終了処理
####################################################

#完了ファイルを生成する（第二引数設定時は作成しない）
if [ $NO_OF_ARGS -eq 1 ]; then
    echo `date '+%Y%m%d_%H%M%S'`": バックアップを正常に終了しました" > ${CURRENT_BACKUP_DIR}${LOG_FILE}
    echo "バックアップ処理を完了しました！！" > ${CURRENT_BACKUP_DIR}${COMPLETE_FILE}
fi
echo すべてのバックアップ処理を完了しました

#メールで完了報告を通知する
if [ ${#ALLOW_ROOT} -eq 0 ]; then
php ${WP_CONTENT_DIR}diskinfo.php ".." "${backup_newsiteurl}" "${admin_email}"
fi