#!/bin/bash
# change_wp_preword-for-conohawing.sh
#
# -----------------------------------------------------------------------------
# Title : リストア処理
# -----------------------------------------------------------------------------
# 2018.10.13 created
#
# Description :
#   backup.shで作成したバックアップファイルからWPのDBとファイルをリストアする
#
# Arguments :
#   [Arg1] WP_CONTENT_DIR
#   [Arg2] 自身のプラグインフォルダ名   
#
# Return value :
#   =0: OK
#   >0: ERROR CODE
#
#
# -----------------------------------------------------------------------------

####################################################
# 初期設定
####################################################

#引数の個数を変数に設定する。
NO_OF_ARGS=$#

# 実行時に指定された引数の数が 1個または2個でなければエラー終了。
if [ $NO_OF_ARGS -ne 2 ]; then
   echo "引数の数が一致しないので終了します。"
   echo "データベース内に記述されているテーブルの接頭辞を変更します。"
   echo "第１引数には変更前の接頭辞を設定してください。"
   echo "第２引数には変更後の接頭辞を設定してください。"
   exit 1
fi
src_preword=$1
dest_preword=$2


#パスを通す
export PATH=$PATH:/usr/local/bin/

#ROOT権限の場合はこのオプションをつけないとエラーになる
ALLOW_ROOT="--allow-root"
#ルート権限を持たないので不要
ALLOW_ROOT=""
shopt -s expand_aliases
alias wp='~/bin/wp-cli.phar'



#################################################
#URLを変更する
#################################################
#URL変更有無を判断する
wp search-replace ${src_preword}commentmeta                   ${dest_precord}commentmeta                 ${ALLOW_ROOT}
wp search-replace ${src_preword}comments                      ${dest_precord}comments                    ${ALLOW_ROOT}
wp search-replace ${src_preword}download_log                  ${dest_precord}download_log                ${ALLOW_ROOT}
wp search-replace ${src_preword}duplicator_packages           ${dest_precord}duplicator_packages         ${ALLOW_ROOT}
wp search-replace ${src_preword}links                         ${dest_precord}links                       ${ALLOW_ROOT}
wp search-replace ${src_preword}liveshoutbox                  ${dest_precord}liveshoutbox                ${ALLOW_ROOT}
wp search-replace ${src_preword}myfavoritesite                ${dest_precord}myfavoritesite              ${ALLOW_ROOT}
wp search-replace ${src_preword}options                       ${dest_precord}options                     ${ALLOW_ROOT}
wp search-replace ${src_preword}postmeta                      ${dest_precord}postmeta                    ${ALLOW_ROOT}
wp search-replace ${src_preword}posts                         ${dest_precord}posts                       ${ALLOW_ROOT}
wp search-replace ${src_preword}pz_linkcard                   ${dest_precord}pz_linkcard                 ${ALLOW_ROOT}
wp search-replace ${src_preword}sitemanager_device            ${dest_precord}sitemanager_device          ${ALLOW_ROOT}
wp search-replace ${src_preword}sitemanager_device_group      ${dest_precord}sitemanager_device_group    ${ALLOW_ROOT}
wp search-replace ${src_preword}sitemanager_device_relation   ${dest_precord}sitemanager_device_relation ${ALLOW_ROOT}
wp search-replace ${src_preword}site_cache                    ${dest_precord}site_cache                  ${ALLOW_ROOT}
wp search-replace ${src_preword}swpm_membership_meta_tbl      ${dest_precord}swpm_membership_meta_tbl    ${ALLOW_ROOT}
wp search-replace ${src_preword}swpm_membership_tbl           ${dest_precord}swpm_membership_tbl         ${ALLOW_ROOT}
wp search-replace ${src_preword}swpm_members_tbl              ${dest_precord}swpm_members_tbl            ${ALLOW_ROOT}
wp search-replace ${src_preword}swpm_payments_tbl             ${dest_precord}swpm_payments_tbl           ${ALLOW_ROOT}
wp search-replace ${src_preword}termmeta                      ${dest_precord}termmeta                    ${ALLOW_ROOT}
wp search-replace ${src_preword}terms                         ${dest_precord}terms                       ${ALLOW_ROOT}
wp search-replace ${src_preword}term_relationships            ${dest_precord}term_relationships          ${ALLOW_ROOT}
wp search-replace ${src_preword}term_taxonomy                 ${dest_precord}term_taxonomy               ${ALLOW_ROOT}
wp search-replace ${src_preword}usermeta                      ${dest_precord}usermeta                    ${ALLOW_ROOT}
wp search-replace ${src_preword}users                         ${dest_precord}users                       ${ALLOW_ROOT}

echo WORDPRESSテーブルの接頭辞${src_preword}を${dest_precord}に変更する処理を完了しました
