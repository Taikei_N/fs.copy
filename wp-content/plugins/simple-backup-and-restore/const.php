<?php
//読み込みは必ずREQUIRE_ONCEを使用すること！！

/**
 * 【注意】プラグインのディレクトリ名とWP登録時に使用するページIDを必ず設定すること！
 * ※プラグインフォルダ名を変更する場合の手順
 * ① プラグインフォルダを変更する。
 * ② 下記のmyPluginDomainをプラグインフォルダ名にする。
 * ③ languagesフォルダのpo/moファイルはプラグインドメイン（ここではフォルダ名と同じ）であるため、変更する。
 * ④  このファイルおよびクラス名もプラグインフォルダに合わしているため必要なら変更する。（必須ではない）
 */

//general definition
define('CONST_VERSION', '1.0');   //ACTIVATION時にDBのOPTIONに出力される
define('CONST_SIMPLE_BACKUP_RESTORE_DOMAIN', 'simple-backup-and-restore');
define('CONST_SIMPLE_BACKUP_RESTORE_DIR', 'simple-backup-and-restore');
define('CONST_SIMPLE_BACKUP_RESTORE_ID', 'simple-backup-and-restore-plugin');
