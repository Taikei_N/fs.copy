<?php
/**
 * Class CommonModule_LoadScripts
 * 投稿画面で使用するJSを対象のショートコードが使われている場合のみ読み込む
 */

if (!class_exists('CommonModule_LoadScripts')) {  //複数のプラグインで使用している場合、重複して読み込まれるのを防ぐ
  class CommonModule_LoadScripts
  {
    private $myPluginDomain;
    private $func_load_admin_scripts;
    private $func_load_post_scripts;
    private $myPluginWPOptionShortcodename;
    private $shortcodeOptions;

    /**
     * CommonModule_LoadScripts constructor.
     * @param $domain　プラグインドメイン名（管理画面で対象プラグインかどうか判定用）
     * @param $callback_adminscript　管理画面用コールバック関数名
     * @param $callback_postscript 投稿画面用コールバック関数名
     * @param $myPluginWPOptionShortcodename ショートコードを保存するためのDBオプション名
     * @param $shortcodeOptions 投稿画面で使用していたら有効にするためのショートコード
     */
    function __construct($domain, $callback_adminscript = false, $callback_postscript = false, $myPluginWPOptionShortcodename = false, $shortcodeOptions = false)
    {
      //debug error_log('CommonModule_LoadScripts: __construct');
      $this->myPluginDomain = $domain;
      if($callback_adminscript || $callback_postscript){
        $this->LoadScripts($callback_adminscript, $callback_postscript, $myPluginWPOptionShortcodename, $shortcodeOptions);
      }
    }
    function LoadScripts($callback_adminscript, $callback_postscript, $myPluginWPOptionShortcodename, $shortcodeOptions)
    {
      $this->func_load_admin_scripts = $callback_adminscript;
      $this->func_load_post_scripts = $callback_postscript;
      $this->myPluginWPOptionShortcodename = $myPluginWPOptionShortcodename;
      $this->shortcodeOptions = $shortcodeOptions;

      //管理画面用コールバック関数が設定されていれば、フックに登録する
      if($callback_adminscript) {
        //管理画面で使用するJSを読み込むための処理をする
        add_action('admin_head', array($this, 'load_admin_scripts_when_targetpage'));
      }
      //投稿画面用コールバック関数が設定されていれば、フックに登録する
      if($callback_postscript) {
        //debug error_log('CommonModule_LoadScripts: add_action(wp_head)');
        //投稿画面で使用するJSを読み込む
        add_action('wp_head', array($this, 'load_post_scripts_when_targetpage'));
      }
      //投稿画面用コールバック関数、設定用オプション、ショートコードが設定されていれば、フックに登録する
      if($callback_postscript
        && $myPluginWPOptionShortcodename
        && $shortcodeOptions) {
        //投稿画面更新時にショートコード使用有無をチェックしてオプションを更新する
        add_action('save_post', array($this, 'save_option_shortcode_post_id_array'));
      }
    }

    /**
     * 管理画面のURLパラメータを判定して管理画面用スクリプトを呼び出す
     */
    function load_admin_scripts_when_targetpage()
    {
      $REQUEST_URI = $_SERVER['REQUEST_URI'];
      if (strpos($REQUEST_URI, $this->myPluginDomain) !== false) {
        //対象プラグインの管理画面なので、管理画面用スクリプトを呼び出す
        if (is_callable($this->func_load_admin_scripts)) {
          //コールバックメソッドを呼び出す
          call_user_func($this->func_load_admin_scripts);
        } else {
          //実行可能なメソッドでないのでエラーを出力する
          error_log($this->myPluginDomain . ': Cannot load scripts in SimpleCommonModule.php (load_admin_scripts_when_targetpage)');
        }
      }
    }

    /**
     * 投稿画面のページがJS読み込み対象かどうか判定して投稿画面用スクリプトを呼び出す
     */
    function load_post_scripts_when_targetpage()
    {
      //debug error_log('CommonModule_LoadScripts: add_action(wp_head): load_post_scripts_when_targetpage');
      if($this->myPluginWPOptionShortcodename) {
        $page_id = get_the_ID();
        //対象投稿画面ページが登録されているDBのOPTIONを取得する
        $wpdb‗option_array = @get_option($this->myPluginWPOptionShortcodename);
      }
      if ($this->myPluginWPOptionShortcodename === false
          || (!empty($wpdb‗option_array) && in_array($page_id, $wpdb‗option_array))) {
        //debug error_log($this->myPluginDomain . ': LOAD OK)');
        if (is_callable($this->func_load_post_scripts)) {
          //コールバックメソッドを呼び出す
          call_user_func($this->func_load_post_scripts);
        } else {
          //実行可能なメソッドでないのでエラーを出力する
          error_log($this->myPluginDomain . ': Cannot load scripts in SimpleCommonModule.php (load_post_scripts_when_targetpage)');
        }
      }
    }

    /**
     * @param $post_id
     */
    function save_option_shortcode_post_id_array($post_id)
    {
      if (wp_is_post_revision($post_id)
        || !in_array(get_post_type($post_id), array('post', 'page'), true)) {
        return;
      }
      //対象のショートコードを設定する
      $postid_array = array();
      //対象のショートコードが含まれているページか否か判定する
      foreach ($this->shortcodeOptions as $option_name) {
        $postid_array = array_merge($postid_array, $this->find_shortcode_occurences($option_name, $post_id));
      }
      //重複キーを除去する
      $postid_array = array_unique($postid_array);
      //DBオプションから対象IDリストを取得する
      $wpdb‗option_array = @get_option($this->myPluginWPOptionShortcodename);
      //DBオプションが空であれば配列を初期化する
      if ($wpdb‗option_array === false) {
        $wpdb‗option_array = array();
      }
      if ((count($postid_array) == 0) && ($key = array_search($post_id, $wpdb‗option_array)) !== false) {
        //対象IDに当プラグインショートコードは含まれないのでリストから削除する
        unset($wpdb‗option_array[$key]);
      } elseif ((count($postid_array) > 0) && ($key = array_search($post_id, $wpdb‗option_array)) === false) {
        //対象IDに当プラグインショートコードが含まれるのでリストに追加する
        $wpdb‗option_array = array_merge($wpdb‗option_array, $postid_array);
      }
      //重複キーを除去する
      $wpdb‗option_array = array_unique($wpdb‗option_array);
      //WPオプションに登録する
      if ($wpdb‗option_array !== false) {
        update_option($this->myPluginWPOptionShortcodename, $wpdb‗option_array);
      } else {
        $autoload = 'yes';
        add_option($this->myPluginWPOptionShortcodename, $wpdb‗option_array, '', $autoload);
      }
    }

    /**
     * @param $shortcode
     * @param $post_id
     * @return array
     */
    function find_shortcode_occurences($shortcode, $post_id)
    {
      $found_ids = array();
      $args = array(
        //'post_type'   => array('page','post'),
        'post_status' => 'publish',
        'posts_per_page' => -1,
      );
      if (get_post_type($post_id) == 'post') {
        $args['p'] = $post_id;
      } else {
        $args['page_id'] = $post_id;
      }
      $query_result = new WP_Query($args);
      foreach ($query_result->posts as $post) {
        if (false !== strpos($post->post_content, $shortcode)) {
          $found_ids[] = $post->ID;
        }
      }
      return $found_ids;
    }

    /**
     * INPUTタグの表示と登録処理（表示サブ関数）
     *
     * 配列データよりINPUTタグを出力、ボタン押下時に登録処理をする
     *
     * @param $ary INPUTタグ出力データ用配列
     *
     */
    function showInputTags($ary){
      ?>
      <table class="form-table">
        <?php
        foreach($ary as $key => $value){
          ?>
          <tr valign="top">
            <th scope="row">
              <label for="inputtext"><?php echo $value['title']?>
              </label>
            </th>
            <td>
              <?php
              //WPオプションに設定された値を取得する
              if(isset($value['key'])){
                $opt = get_option($value['key']);
                $itemval = isset($opt) ? $opt: null;
              }

              if($value['type'] == 'title') {
              } else if($value['type'] == 'textarea') {
                $opt = '';
                $itemval = '';
                if(isset($value['key'])){
                  $opt = get_option($value['key']);
                  $itemval = isset($opt) ? $opt: null;
                  //バックスラッシュを削除する
                  $itemval = str_replace('\\', '',$itemval);
                  if(isset($value['array']) && $value['array']){
                    $blanket = '[]';
                  }else{
                    $blanket = '';
                  }
                }
                ?>
                <textarea name="<?php echo $value['key'].$blanket; ?>"
                          type="text"
                          id="<?php echo 'textarea_' . $value['key']; ?>"
                <?php if(isset($value['disabled']) && $value['disabled'] == true) echo 'disabled'; ?>
                     rows="<?php echo $value['rows']; ?>"
                          cols="<?php echo $value['cols']; ?>"
                          class="regular-text" ><?php echo $itemval; ?></textarea>

                <?php
              } else if($value['type'] == 'text') {
                $opt = '';
                $itemval = '';
                if(isset($value['key'])){
                  $opt = get_option($value['key']);
                  $itemval = isset($opt) ? $opt: null;
                  if($value['key'] == 'backup_srchomepath'){
                    $backup_srchomepath = $itemval;
                  }elseif($value['key'] == 'backup_desthomepath'){
                    $backup_desthomepath = $itemval;
                  }elseif($value['key'] == 'backup_tempdirname'){
                    $backup_tempdirname = $itemval;
                  }
                }
                ?>
                <input name="<?php echo $value['key']; ?>"
                       type="text"
                       id="<?php echo 'text_' . $value['key']; ?>"
                  <?php if(isset($value['disabled']) && $value['disabled'] == true) echo 'disabled'; ?>
                       value="<?php echo $itemval; ?>"
                       size="<?php echo $value['size']; ?>"
                       maxlength="<?php echo $value['maxlength']; ?>"
                       class="regular-text" />
                <?php
              } elseif($value['type'] == 'radio') {
                $opt = '';
                $itemval = '';
                if(isset($value['key'])){
                  $opt = get_option($value['key']);
                  $itemval = isset($opt) ? $opt: null;
                  if($value['key'] == 'backup_actionmode'){
                    $backup_actionmode = $itemval;
                  }
                }
                //echo '************************************************************VAL='.$itemval.'<br>';
                foreach($value['content'] as $key_title => $value_title){
                  ?>
                  <input name="<?php echo $value['key']; ?>"
                         type="radio"
                         id="<?php echo 'radio_' . $value['key'] . strval($key_title); ?>"
                         value="<?php echo strval($key_title); ?>"
                    <?php if($key_title == $itemval) echo 'checked="checked"'; ?>
                         class="regular-radio"><?php echo $value_title; ?></input>
                  <?php
                }
              } elseif($value['type'] == 'label') {
//                $itemval = '';
//                if($value['key'] == 'backup_tempwpcontentpath'){
//                  if($backup_actionmode == 2){
//                    //復元モードの時はWP-CONTENTの解凍先を表示する
//                    $itemval = $backup_desthomepath . $backup_tempdirname . $backup_srchomepath;
//                  } else {
//                    //復元モード以外は未使用のため非表示
//                    $itemval = 'バックアップモードのため未使用';
//                  }
//                }
                ?>
                <label for="inputtext"><?php echo $itemval; ?></label>
                <?php
              }
              ?>
            </td>
          </tr>
          <?php
        }
        ?>

      </table>
      <?php
    }


    /**
     * INPUTタグの表示と登録処理（登録サブ関数）
     *
     * 配列データよりINPUTタグを出力、ボタン押下時に登録処理をする
     *
     * @param $ary
     */
    function updateInputTags($ary){
      foreach($ary as $key => $value){
        if(isset($value['key'])
          && $value['key'] !='label'){
          if ( isset($_POST[$value['key']])) {
            check_admin_referer('shoptions');
            $opt = $_POST[$value['key']];
            update_option($value['key'], $opt);
            $savedflg = true;
          }
        }
      }
    }



  }
}