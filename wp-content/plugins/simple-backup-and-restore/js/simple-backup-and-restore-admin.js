//2021.6.26 DEL jQueryのloadが効かないため削除
//jQuery(function($){
  // この中であれば「$」使用可

  jQuery( ".tooltip" ).tooltip( {
    tooltipClass:"tooltipClass",
    position: {
      my: 'left+240 top', at: 'left bottom', collision: 'flip'
      // my: "center top",
      // at: "center bottom"
    },
    show: {
      effect: "drop",
      delay: 300
    },
    hide: {
      effect: "drop",
      delay: 300
    },
    track: false
  } );

  //=================================================
  // 実行前確認ダイアログ
  //=================================================
  function checkbeforeexec(scriptname){
    // 「OK」時の処理開始 ＋ 確認ダイアログの表示
    if(window.confirm(scriptname+'を実行します。よろしいですか？')){
      // 「OK」時の処理終了
      return true;
    }else{
      // 「キャンセル」時の処理開始
      window.alert(scriptname+'は実行前にキャンセルされました。'); // 警告ダイアログを表示
      return false;
    }
  }

  //=================================================
  // インターバルタイマー設定
  //=================================================
  var timer;
  //タイマー開始関数
  function startAjax(inertval){
    //inertvalの秒数毎にsyori関数を実行する。その情報をtimer変数へ入れている。
    timer = setInterval(ajax_getstatus, inertval);
  }

  //=================================================
  // ステータス情報（ファイル）をクリアする
  //=================================================
  function clearstatus(){
    //ステータスファイルをクリアする。
    ajax_clearstatus();
  }

  //=================================================
  //ページリロード時の処理
  //=================================================
  jQuery(window).on('load', function(){
  if(SimpleBackupRestoreScriptData.debugMode) {
    //デバッグモードではtextareaをjQueryで自動的にスクロールさせるコード
    var textarea = jQuery("textarea.regular-text");
    textarea.scrollTop(textarea[0].scrollHeight);
  }
    // var textarea2 = jQuery("textarea#statusmessage");
    // textarea2.scrollTop(textarea2[0].scrollHeight);

  //起動時にステータス情報を表示する
  ajax_getstatus();
  if(SimpleBackupRestoreScriptData.ajaxMode == "3"){
      //タイマー開始（AJAX通信１０ｓ間隔）
      startAjax(10000);
    }else if(SimpleBackupRestoreScriptData.ajaxMode == "2"){
      //タイマー開始（AJAX通信３ｓ間隔）
      startAjax(3000);
    }else if(SimpleBackupRestoreScriptData.ajaxMode == "1"){
      //タイマー開始（AJAX通信１ｓ間隔）
      startAjax(1000);
    }else if(SimpleBackupRestoreScriptData.ajaxMode == "5"){
	  //タイマー開始（AJAX通信１m間隔）
	  startAjax(60000);
    }else if(SimpleBackupRestoreScriptData.ajaxMode == "4"){
	  //タイマー開始（AJAX通信３０ｓ間隔）
	  startAjax(30000);
    }

  });


  //=================================================
  //クリアボタンが押されるとステータスファイルクリア処理を呼び出す
  //=================================================
  jQuery("input:button[name=ajax_clear]").click(clearstatus);




  //=================================================
  // ステータスファイルクリア要求をする（AJAXトリガ）
  //=================================================
  function ajax_clearstatus(){
    var cmd='clear_status';
    jQuery.ajax({
      type: 'GET',
      url: SimpleBackupRestoreScriptData.api,
      data: {
        'action' : SimpleBackupRestoreScriptData.action,
        'cmd' : cmd,
      },
      //ajax通信エラー
      error : function(XMLHttpRequest, textStatus, errorThrown) {
        //   // エラー時のデバッグ用
        ShowStatusMsg("ToT");
        console.log("ajax通信に失敗しました(2)");
        console.log(XMLHttpRequest);
        console.log(textStatus);
        console.log(errorThrown);
      },
      success: function( callback ){
        if ( callback != null && callback.status == 'OK' ) {
          ShowStatusMsg("--:--:--", "ステータス情報をクリアしました", callback.result);
        } else {
          ShowStatusMsg("処理は失敗しました(1)");
        }
      }
    });
    return false;
  }

  //=================================================
  // ステータス情報を取得する（AJAXトリガ）
  //=================================================
  function ajax_getstatus(){
    var cmd='get_status';
    jQuery.ajax({
      type: 'GET',
      url: SimpleBackupRestoreScriptData.api,
      data: {
        'action' : SimpleBackupRestoreScriptData.action,
        'cmd' : cmd,
      },
      //ajax通信エラー
      error : function(XMLHttpRequest, textStatus, errorThrown) {
        clearInterval();
        //   // エラー時のデバッグ用
        ShowStatusMsg("ToT");
        console.log("ajax通信に失敗しました(2)");
        console.log(XMLHttpRequest);
        console.log(textStatus);
        console.log(errorThrown);
      },
      success: function( callback ){
        clearInterval();
        if ( callback != null && callback.status == 'OK' ) {
          ShowStatusMsg(callback.time, callback.complete, callback.result);
        } else {
          ShowStatusMsg("--:--:--", "処理は失敗しました(1)");
        }
      }
    });
    return false;

    //参考：別の呼び出し方
    // jQuery.ajax({
    //   type    : "GET",
    //   url     : "admin-ajax.php?action=my_ajax",
    //   dataType: "json",
    //   async   : true
    //
    // }).done(function(callback){
    //
    //   if ( callback != null && callback.status == 'OK' ) {
    //     ShowStatusMsg(callback.result);
    //   } else {
    //     ShowStatusMsg("処理は失敗しました(1)");
    //   }
    //
    // }).fail(function(XMLHttpRequest, textStatus, errorThrown){
    //
    //   ShowStatusMsg("処理は失敗しました(2)");
    //
    //   // エラー時のデバッグ用
    //   //console.log(XMLHttpRequest);
    //   //console.log(textStatus);
    //   //console.log(errorThrown);
    //
    // });

    return;

  }

  //=================================================
  // ステータス情報（AJAX）取得後にメッセージを表示する
  //=================================================
  function ShowStatusMsg(tm,complete, msg) {

    jQuery("span#statustime").empty();
    jQuery("span#statustime").append(tm);
    if(complete){
      //メッセージボックスの内容を更新する
      jQuery('#message_backup_complete').empty();
      jQuery('#message_backup_complete').append(complete);
      jQuery('#message_backup_complete').dialog('open');
    }else{
      //ステータス情報を更新する
      jQuery("#statusmessage").empty();
      jQuery("#statusmessage").append(msg);
    }
    return;

  }

  //=================================================
  // JQUERY UIダイアログの初期設定
  //=================================================
  jQuery('#message_backup_complete').dialog({
    autoOpen: false,
    title: '処理完了確認',
    closeOnEscape: false,
    modal: true,
    buttons: {
      "OK": function(){
        location.reload();
        jQuery(this).dialog('close');
      }
    }
  });

//2021.6.26 DEL
//});

