<?php
/**
 * Class myPluginInitClass
 *
 * バックアップリストアプラグインの有効化時の初期化処理をします
 *
 * @author H.Matsubara
 * @version 0.01
 * @copyright Sincere Technology
 */

if(!class_exists('myPluginActivationClass')) {
  class myPluginActivationClass
  {
    //バックアップファイル名（データベース）
    const NAME_BACKUP_DATABASE = '${SYSDATE}MYSQL_DATABASE.sql';
    //バックアップファイル名（テーマフォルダ）
    const NAME_BACKUP_WP_THEMES = '${SYSDATE}WP_THEMES.tar.gz';
    //バックアップファイル名（プラグインフォルダ）
    const NAME_BACKUP_WP_PLUGINS = '${SYSDATE}WP_PLUGINS.tar.gz';
    //バックアップファイル名（アップロードフォルダ）
    const NAME_BACKUP_WP_UPLOADS = '${SYSDATE}WP_UPLOADS.tar.gz';
    //バックアップファイル名（アップロードフォルダ）
    const NAME_BACKUP_WP_CONFIG = '${SYSDATE}wp-config.php';
    //バックアップファイル名（アップロードフォルダ）
    const NAME_BACKUP_WP_INFODATA = '${SYSDATE}BACKUP_INFODATA.txt';
    /**
     * @var $instance Instance インスタンス変数
     */
    protected static $instance = null;

    function __construct()
    {
      //定数宣言
      require_once(plugin_dir_path( __FILE__ ).'const.php');
    }

    /**
     * インスタンス化を実行する（コンストラクターを使用しない場合はメソッド内で呼び出すことで可能)
     *
     * WPのプラグインインストール時に起動されるフックに登録する
     */
    public static function get_instance() {
      if (null == self::$instance) {
        self::$instance = new self;
      }

      return self::$instance;
    }

    /**
     * バックアップリストアプラグインのプラグインインストール時実行関数
     *
     * WPのプラグインインストール時に起動されるフックに登録する
     */
    function activation()
    {
      //self::get_instance();
      //プラグインバージョンを出力する
      update_option('backup_version', CONST_VERSION);
      $this->copyBackupRestoreShellScript();
      //$this->myDatabase->install();
    }

    /**
     * 実行用スクリプトのコピー
     *
     * バックアップ・リストア実行用シェルスクリプトをWP-CONTENT直下にコピーする
     * NOTE: PLUGINフォルダはリストア時に消去されるためWP-CONTENT内で実行する
     *
     * @return bool
     */
    private function copyBackupRestoreShellScript()
    {
      //バックアップスクリプトをWP-CONTENTフォルダに設置
      $ret = @copy(WP_PLUGIN_DIR . "/" . CONST_SIMPLE_BACKUP_RESTORE_DIR . "/backup.sh", WP_CONTENT_DIR . "/backup.sh");
      @chmod(WP_CONTENT_DIR . "/backup.sh", 0777);
      //共通スクリプトをWP-CONTENTフォルダに設置
      $ret = @copy(WP_PLUGIN_DIR . "/" . CONST_SIMPLE_BACKUP_RESTORE_DIR . "/backup_option.sh", WP_CONTENT_DIR . "/backup_option.sh");
      @chmod(WP_CONTENT_DIR . "/backup_option.sh", 0777);
      //メール通知用PHPスクリプトをWP-CONTENTフォルダに設置
      $ret = @copy(WP_PLUGIN_DIR . "/" . CONST_SIMPLE_BACKUP_RESTORE_DIR . "/diskinfo.php", WP_CONTENT_DIR . "/diskinfo.php");
      @chmod(WP_CONTENT_DIR . "/diskinfo.php", 0777);
      //リストアスクリプトをWP-CONTENTフォルダに設置
      $ret = @copy(WP_PLUGIN_DIR . "/" . CONST_SIMPLE_BACKUP_RESTORE_DIR . "/restore.sh", WP_CONTENT_DIR . "/restore.sh");
      @chmod(WP_CONTENT_DIR . "/restore.sh", 0777);
      //テスト用スクリプトをWP-CONTENTフォルダに設置
      $ret = @copy(WP_PLUGIN_DIR . "/" . CONST_SIMPLE_BACKUP_RESTORE_DIR . "/script-test.sh", WP_CONTENT_DIR . "/script-test.sh");
      @chmod(WP_CONTENT_DIR . "/script-test.sh", 0777);
      //WP-CLIスクリプトをWP-CONTENTフォルダに設置
      $ret = @copy(WP_PLUGIN_DIR . "/" . CONST_SIMPLE_BACKUP_RESTORE_DIR . "/wp-cli.phar", WP_CONTENT_DIR . "/wp-cli.phar");
      @chmod(WP_CONTENT_DIR . "/wp-cli.phar", 0777);

      //バックアップフォルダが未設定時は名前をBACKUPとする
      if (strlen(@get_option('backup_srcdirname')) == 0) {
        update_option('backup_srcdirname', 'backup');
      }
      $dir_src = ABSPATH . @get_option('backup_srcdirname');
      if (file_exists($dir_src) == false) {
        @mkdir($dir_src, 0777);
      }
      @chown($dir_src, "apache");
      @chgrp($dir_src, "apache");
      @chmod($dir_src, 0777);

      //リストアフォルダが未設定時は名前をRESTOREとする
      if (strlen(@get_option('backup_destdirname')) == 0) {
        update_option('backup_destdirname', 'restore');
      }
      $dir_dest = ABSPATH . @get_option('backup_destdirname');
      if (file_exists($dir_dest) == false) {
        @mkdir($dir_dest, 0777);
      }
      @chown($dir_dest, "apache");
      @chgrp($dir_dest, "apache");
      @chmod($dir_dest, 0777);

      //受信データ用フォルダが未設定時は名前をRESTOREとする
      if (strlen(@get_option('backup_recieveddirname')) == 0) {
        update_option('backup_recieveddirname', 'recieved');
      }
      $dir_recieved = ABSPATH . @get_option('backup_recieveddirname');
      if (file_exists($dir_recieved) == false) {
        @mkdir($dir_recieved, 0777);
      }
      @chown($dir_recieved, "apache");
      @chgrp($dir_recieved, "apache");
      @chmod($dir_recieved, 0777);

      //ゴミ箱フォルダが未設定時は名前をtrash_boxとする
      if (strlen(@get_option('backup_trashboxdirname')) == 0) {
        update_option('backup_trashboxdirname', 'trash_box');
      }
      $dir_trashbox = ABSPATH . @get_option('backup_trashboxdirname');
      if (file_exists($dir_trashbox) == false) {
        @mkdir($dir_trashbox, 0777);
      }
      @chown($dir_trashbox, "apache");
      @chgrp($dir_trashbox, "apache");
      @chmod($dir_trashbox, 0777);

      //一時フォルダが未設定時は名前をtempとする
      if (strlen(@get_option('backup_tempdirname')) == 0) {
        update_option('backup_tempdirname', 'temp');
      }
      $dir_temp = ABSPATH . @get_option('backup_tempdirname');
      if (file_exists($dir_temp) == false) {
        @mkdir($dir_temp, 0777);
      }
      @chown($dir_temp, "apache");
      @chgrp($dir_temp, "apache");
      @chmod($dir_temp, 0777);


      //データベース名デフォルト値設定
      if (strlen(@get_option('backup_fnamemysqldata')) == 0) {
        update_option('backup_fnamemysqldata', self::NAME_BACKUP_DATABASE);
      }
      //WPテーマファイルデフォルト値設定
      if (strlen(@get_option('backup_fnamewpthemes')) == 0) {
        update_option('backup_fnamewpthemes', self::NAME_BACKUP_WP_THEMES);
      }
      //WPプラグインファイルデフォルト値設定
      if (strlen(@get_option('backup_fnamewpplugins')) == 0) {
        update_option('backup_fnamewpplugins', self::NAME_BACKUP_WP_PLUGINS);
      }
      //WPアップロードファイルデフォルト値設定
      if (strlen(@get_option('backup_fnamewpuploads')) == 0) {
        update_option('backup_fnamewpuploads', self::NAME_BACKUP_WP_UPLOADS);
      }
      //WPバックアップ情報ファイルデフォルト値設定
      if (strlen(@get_option('backup_fnameinfodata')) == 0) {
        update_option('backup_fnamewpconfig', self::NAME_BACKUP_WP_CONFIG);
      }
      //WPバックアップ情報ファイルデフォルト値設定
      if (strlen(@get_option('backup_fnameinfodata')) == 0) {
        update_option('backup_fnameinfodata', self::NAME_BACKUP_WP_INFODATA);
      }

      //スケジューラスクリプトをWP-CONTENTフォルダに設置
      $ret = @copy(WP_PLUGIN_DIR . "/" . CONST_SIMPLE_BACKUP_RESTORE_DIR . "/scheduler.sh", WP_CONTENT_DIR . "/scheduler.sh");
      @chmod(WP_CONTENT_DIR . "/scheduler.sh", 0777);
      //SSH転送スクリプトをWP-CONTENTフォルダに設置
      $ret = @copy(WP_PLUGIN_DIR . "/" . CONST_SIMPLE_BACKUP_RESTORE_DIR . "/ssh_transfer.sh", WP_CONTENT_DIR . "/ssh_transfer.sh");
      @chmod(WP_CONTENT_DIR . "/ssh_transfer.sh", 0777);
      //SCHEDULER.SHをCRONTABに登録する
      $exec_cmd = WP_CONTENT_DIR . "/scheduler.sh " . WP_CONTENT_DIR . " >> " . WP_CONTENT_DIR . "/scheduler_sh.log";

      //デフォルトバックアップファイルが存在する場合はRESTOREフォルダに設置
	  $tgtfilenames = array("00000000_000000_BACKUP_INFODATA.txt"
						  ,"00000000_000000_WP_UPLOADS.tar.gz"
						  ,"00000000_000000_wp-config.php"
						  ,"00000000_000000_WP_PLUGINS.tar.gz"
						  ,"00000000_000000_WP_THEMES.tar.gz"
						  ,"00000000_000000_MYSQL_DATABASE.sql");
	  foreach ($tgtfilenames as $tgtfilename) {
		  if (file_exists($tgtfilename) == false) {
			  $ret = @copy(WP_PLUGIN_DIR . "/" . $tgtfilename, $dir_recieved . $tgtfilename);
		  }
	  }

      //TODO: すでにクローンが設定済みの場合上書きされるためコメントアウト。設定有無を判定して登録できるようにしたい
      //$this->register_cron("", "*:*/10", "", "DAY", $exec_cmd);
      return $ret;
    }

    /**
     * CRON登録
     *
     * 毎月/毎週/毎日 実行されるコマンドを cron に登録する
     *
     * @param $date 毎月実行を指定した場合に実行される日を指定
     * @param $time 毎日実行を指定した場合に実行される時間を指定
     * @param $dayofweek 毎週実行を指定した場合に実行される曜日を指定
     * @param $frequency 実行の頻度 ("MONTH" | "WEEK" | "DAY")
     * @param $command 実行するコマンド
     * @return string
     */
    function register_cron($date, $time, $dayofweek, $frequency, $command)
    {
      if (($cron = popen("crontab -", "w"))) {
        fputs($cron, $this->_cronline($date, $time, $dayofweek, $frequency, $command));

        pclose($cron);
      }
    }

    /**
     * CRONTAB登録用の文字列生成
     *
     * CRONTAB登録用の文字列を生成する
     *
     * @param $date
     * @param $time
     * @param $dayofweek
     * @param $frequency
     * @param $command
     * @return string
     */
    function _cronline($date, $time, $dayofweek, $frequency, $command)
    {

      $name = array(
        "MONTH" => array("day", $date),
        "WEEK" => array("dow", $dayofweek),
        "DAY" => array("dummy", null),
      );

      /* デフォルトは '*' */
      $day = $month = $dow = "*";

      /*
       * MONTH の場合は $day に $date を代入
       * WEEK の場合は $dow に $dayofweek を代入
       * DAY の場合は未使用なので $dummy とする
       */
      ${$name[$frequency][0]} = preg_replace("!.*/!", "", $name[$frequency][1]);
      list($hour, $min) = explode(":", $time);
      return (sprintf("%s  %s  %s  %s  %s  %s\n",
        $min, $hour, $day, $month, $dow, $command));

    }

    /**
     * バックアップリストアプラグインのプラグインアンインストール時実行関数
     *
     * WPのプラグインアンインストール時に起動されるフックに登録する
     */
    function deactivation()
    {
      //$this->myDatabase->uninstall();
    }

  }
}
