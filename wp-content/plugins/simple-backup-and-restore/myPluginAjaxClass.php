<?php
/**
 * Created by PhpStorm.
 * User: matub
 * Date: 2018/11/21
 * Time: 23:57
 */

if(!class_exists('myPluginAjaxClass')) {
  class myPluginAjaxClass
  {
    const statusfile = 'backup-process-status.txt';
    const statusfilepath = WP_CONTENT_DIR . "/" . self::statusfile;
    const completefile = 'complete.txt';
    const completefilepath = WP_CONTENT_DIR . "/" . self::completefile;

    /**
     * AJAXに対応する
     */
    function my_ajax()
    {

      if (isset($_GET["cmd"])) {
        $cmd = $_GET["cmd"];
      } else {
        $cmd = false;
      }
      header('Content-Type: application/json; charset=utf-8');

      /* 処理の判定 */
      if ($cmd) {

        $dt = date("H:i:s");
        if ($cmd == "get_status") {
          //echo json_encode( array( 'status' => 'OK','result'=>'いまの時刻は '. date("H:i:s") ) );
          if (file_exists(self::statusfilepath)) {
            $status = nl2br(@file_get_contents(self::statusfilepath));
          } else {
            $status = "ただいまスクリプトは実行されておりません";
          }
          //ファイルの存在チェックをする
          $completestatus = file_exists(self::completefilepath);
          if ($completestatus) {
            //ファイルが存在する場合は中身を取得する
            $completestatus = @file_get_contents(self::completefilepath);
            //TODO: KUSANAGI環境ではWP-CONTENT以下のファイルは削除できない。TEMPディレクトリに置くようにすれば可能。
            unlink(self::completefilepath);
          }
          echo json_encode(array('status' => 'OK', 'time' => $dt, 'complete' => $completestatus, 'result' => $status));
        } elseif ($cmd == "clear_status") {
          unlink(self::statusfilepath);
          unlink(self::completefilepath);
          $status = "クリアされました";
          echo json_encode(array('status' => 'OK', 'time' => $dt, 'result' => $status));
        }

      } else {
        //CMDが未設定の場合はNGを返す
        echo json_encode(array('status' => 'NG'));
      }

      die();

    }
  }
}