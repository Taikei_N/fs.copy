<?php
/**
 * Class myPluginMainClass
 *
 * バックアップリストアプラグインの機能を実装します
 *
 * @author H.Matsubara
 * @version 0.01
 * @copyright Sincere Technology
 */


if(!class_exists('myPluginMainClass')) {
  class myPluginMainClass {

    const ADD_SCRIPT_FILE_SAVED_DBOPTION = 'backup_addtional_script_saved.txt';
    const ADD_SCRIPT_FILE_EXECUTABLE = 'backup_addtional_script.sh';

    private $debugMode;
    private $ajaxMode;

    /**
   * @var myPluginFileUtilClass ファイル関連機能実装クラス
   */
    private $myPluginFileUtil;

    /**
    * @var myPluginDatabaseClass
    */
    private $myPluginDatabase;

    private $production_domain;

    private $pre_command_sudo;

    private $CommonModule_LoadScripts;


    //設定
    const itemary = array(
                array('type'=>'title', 'title'=> '<font color="blue">フォルダ名設定</font>')
                ,array('type'=>'text', 'key'=>'backup_srcdirname','title'=>'バックアップディレクトリ名','size'=>32,'maxlength'=>64)
                ,array('type'=>'text', 'key'=>'backup_destdirname','title'=>'リストアディレクトリ名','size'=>32,'maxlength'=>64)
                ,array('type'=>'text', 'key'=>'backup_recieveddirname','title'=>'受信データ用ディレクトリ名','size'=>32,'maxlength'=>64)
                ,array('type'=>'text', 'key'=>'backup_trashboxdirname','title'=>'ゴミ箱ディレクトリ名','size'=>32,'maxlength'=>64)
                ,array('type'=>'text', 'key'=>'backup_tempdirname','title'=>'一時ディレクトリ名','size'=>32,'maxlength'=>64)
                //,array('type'=>'label','key'=>'backup_tempwpcontentpath', 'title'=>'ワーク内解凍WP-CONTENTパス')
                ,array('type'=>'title', 'title'=> '<font color="blue">ファイル名設定</font>')
                ,array('type'=>'text', 'key'=>'backup_fnamemysqldata','title'=>'データベース圧縮ファイル名','size'=>32,'maxlength'=>64,'disabled'=>false)
                ,array('type'=>'text', 'key'=>'backup_fnamewpthemes','title'=>'WPテーマ圧縮ファイル名','size'=>32,'maxlength'=>64,'disabled'=>false)
                ,array('type'=>'text', 'key'=>'backup_fnamewpplugins','title'=>'WPプラグイン圧縮ファイル名','size'=>32,'maxlength'=>64,'disabled'=>false)
                ,array('type'=>'text', 'key'=>'backup_fnamewpuploads','title'=>'WPアップロード圧縮ファイル名','size'=>32,'maxlength'=>64,'disabled'=>false)
                ,array('type'=>'text', 'key'=>'backup_fnamewpconfig','title'=>'wp-config.phpファイル名','size'=>32,'maxlength'=>64,'disabled'=>false)
                ,array('type'=>'text', 'key'=>'backup_fnameinfodata','title'=>'バックアップ情報ファイル名','size'=>32,'maxlength'=>64,'disabled'=>false)
                ,array('type'=>'title', 'title'=> '<font color="blue">本番環境設定</font>')
                ,array('type'=>'text', 'key'=>'backup_production_dmain','title'=>'ドメイン名','size'=>32,'maxlength'=>64,'disabled'=>false)
                ,array('type'=>'radio', 'key'=>'backup_wpcli_alias','title'=>'WP-CLIコマンドエイリアス指定', 'content'=> array(0=>'しない',1=>'する'))
                ,array('type'=>'text', 'key'=>'backup_php_path','title'=>'WP-CLIを起動するPHPパスを指定','size'=>32,'maxlength'=>64,'disabled'=>false)
                ,array('type'=>'radio', 'key'=>'backup_allow_root','title'=>'ルート権限有無', 'content'=> array(0=>'なし',1=>'あり'))
                ,array('type'=>'textarea', 'key'=>'backup_addtional_script','title'=>'バッチ用オプションコード','rows'=>20,'cols'=>40,'disabled'=>false)
                ,array('type'=>'title', 'title'=> '<font color="blue">開発環境設定</font>')
                ,array('type'=>'radio', 'key'=>'backup_debugmode','title'=>'ログ出力', 'content'=> array(0=>'しない',1=>'する'))
                ,array('type'=>'radio', 'key'=>'backup_ajaxmode','title'=>'AJAX通信設定（ステータス情報表示）', 'content'=> array(0=>'しない',1=>'1秒',2=>'3秒',3=>'10秒',4=>'30秒',5=>'1分'))
                ,array('type'=>'radio', 'key'=>'backup_vagrantmode','title'=>'Vagrantモード', 'content'=> array(0=>'いいえ',1=>'はい'))
  //							,array('type'=>'title', 'title'=> '<font color="blue">その他</font>')
  //							,array('type'=>'text', 'key'=>'backup_newsiteurl','title'=>'リストア後URL変更用URL','size'=>32,'maxlength'=>64)
    );

    //スケジューラ設定（毎時）
    const scd_houritemary = array(
                array('type'=>'title', 'title'=> '<font color="blue">タイマー設定</font>')
                ,array('type'=>'text', 'key'=>'backup_scd_minute','title'=>'分','size'=>2,'maxlength'=>2)
                ,array('type'=>'label','key'=>'backup_scd_target', 'title'=>'次回バックアップ起動日時')
                ,array('type'=>'title', 'title'=> '<font color="blue">機能設定</font>')
                ,array('type'=>'radio', 'key'=>'backup_scd_execmode','title'=>'自動実行モード', 'content'=> array(0=>'バックアップ',1=>'サーバデータ受信',2=>'サーバデータ受信＋リストア'))
    );

    //スケジューラ設定（毎日）
    const scd_dayitemary = array(
                array('type'=>'title', 'title'=> '<font color="blue">タイマー設定</font>')
                ,array('type'=>'text', 'key'=>'backup_scd_hour','title'=>'時','size'=>2,'maxlength'=>2)
                ,array('type'=>'text', 'key'=>'backup_scd_minute','title'=>'分','size'=>2,'maxlength'=>2)
                ,array('type'=>'label','key'=>'backup_scd_target', 'title'=>'次回バックアップ起動日時')
                ,array('type'=>'title', 'title'=> '<font color="blue">機能設定</font>')
                ,array('type'=>'radio', 'key'=>'backup_scd_execmode','title'=>'自動実行モード', 'content'=> array(0=>'バックアップ',1=>'サーバデータ受信',2=>'サーバデータ受信＋リストア'))
    );

    //スケジューラ設定（毎週）
    const scd_weekitemary = array(
                array('type'=>'title', 'title'=> '<font color="blue">タイマー設定</font>')
                ,array('type'=>'radio', 'key'=>'backup_scd_day','title'=>'曜日', 'content'=> array(0=>'日',1=>'月',2=>'火',3=>'水',4=>'木',5=>'金',6=>'土','*'=>'指定しない'))
                ,array('type'=>'text', 'key'=>'backup_scd_hour','title'=>'時','size'=>2,'maxlength'=>2)
                ,array('type'=>'text', 'key'=>'backup_scd_minute','title'=>'分','size'=>2,'maxlength'=>2)
                ,array('type'=>'label','key'=>'backup_scd_target', 'title'=>'次回バックアップ起動日時')
                ,array('type'=>'title', 'title'=> '<font color="blue">機能設定</font>')
                ,array('type'=>'radio', 'key'=>'backup_scd_execmode','title'=>'自動実行モード', 'content'=> array(0=>'バックアップ',1=>'サーバデータ受信',2=>'サーバデータ受信＋リストア'))
    );

    //スケジューラ設定（毎月）
    const scd_monthitemary = array(
                array('type'=>'title', 'title'=> '<font color="blue">タイマー設定</font>')
                ,array('type'=>'text', 'key'=>'backup_scd_date','title'=>'日','size'=>2,'maxlength'=>2)
                ,array('type'=>'text', 'key'=>'backup_scd_hour','title'=>'時','size'=>2,'maxlength'=>2)
                ,array('type'=>'text', 'key'=>'backup_scd_minute','title'=>'分','size'=>2,'maxlength'=>2)
                ,array('type'=>'label','key'=>'backup_scd_target', 'title'=>'次回バックアップ起動日時')
                ,array('type'=>'title', 'title'=> '<font color="blue">機能設定</font>')
                ,array('type'=>'radio', 'key'=>'backup_scd_execmode','title'=>'自動実行モード', 'content'=> array(0=>'バックアップ',1=>'サーバデータ受信',2=>'サーバデータ受信＋リストア'))
    );

    //SSH関連設定
    const ssh_itemary = array(
              array('type'=>'title', 'title'=> '<font color="blue">ssh関連設定（データ受信相手先サーバー情報設定）</font>')
              ,array('type'=>'text', 'key'=>'backup_srchomepath','title'=>'相手先ドキュメントルートパス','size'=>32,'maxlength'=>128)
              ,array('type'=>'text', 'key'=>'backup_sshsrchostname','title'=>'相手先ホスト名','size'=>32,'maxlength'=>64)
              ,array('type'=>'text', 'key'=>'backup_sshsrcportno','title'=>'相手先ポートNo','size'=>32,'maxlength'=>64)
              ,array('type'=>'text', 'key'=>'backup_sshsrcusername','title'=>'相手先ユーザー名','size'=>32,'maxlength'=>64)
              ,array('type'=>'text', 'key'=>'backup_sshsrcprivatekeypath','title'=>'相手先サーバー接続用秘密鍵 フォルダ名','size'=>32,'maxlength'=>128)
              ,array('type'=>'text', 'key'=>'backup_sshsrcprivatekeyname','title'=>'相手先サーバー接続用秘密鍵 ファイル名','size'=>32,'maxlength'=>64)
              ,array('type'=>'radio', 'key'=>'backup_sshnotification','title'=> '受信完了通知', 'content'=> array(1=>'する',0=>'しない'))
    );


     /*****************************************************************************
    * myPluginMainClass constructor.
    *
    *  ファイル関連クラスの読み込みおよびインスタンス化をする
    *
    ******************************************************************************/
     function __construct() {
        //定数宣言
        require_once(plugin_dir_path( __FILE__ ).'const.php');
        //グローバル変数宣言（※補完されない）
        require(plugin_dir_path( __FILE__ ).'global.php');
        //$this->myPluginDatabase = $GlobalmyPluginDatabase;
        $this->CommonModule_LoadScripts = $GlobalCommonModule_LoadScripts;
        $this->debugMode = @get_option("backup_debugmode");
        $this->ajaxMode = @get_option("backup_ajaxmode");
        $this->production_domain = @get_option("backup_production_dmain");
        $this->pre_command_sudo = @get_option("backup_vagrantmode") == "1" ? "echo 'vagrant' | sudo -S sh " : "";

        //ファイル関連処理クラスの読み込みとインスタンス化をする
        require_once(plugin_dir_path( __FILE__ )."myPluginFileUtilClass.php");
        $this->myPluginFileUtil = new myPluginFileUtilClass();
    }

    /*****************************************************************************
    * バックアップリストアプラグインの実装部メイン関数
   *
   * 管理画面のメニューバーに追加する（add_menu_pageに当関数を登録する）
   *
    ******************************************************************************/
    public function main()
    {
         //バックアップタブがクリックされた場合
         $active_backup = ( isset( $_GET['tabclicked'] ) && 'backup' == $_GET['tabclicked'] ) ? true : false;
         //リストアタブがクリックされた場合
         $active_restore = ( isset( $_GET['tabclicked'] ) && 'restore' == $_GET['tabclicked'] ) ? true : false;
         //転送タブがクリックされた場合
         $active_transfer = ( isset( $_GET['tabclicked'] ) && 'transfer' == $_GET['tabclicked'] ) ? true : false;
         //Schedulerタブがクリックされた場合
         $active_scheduler = ( isset( $_GET['tabclicked'] ) && 'scheduler' == $_GET['tabclicked'] ) ? true : false;
         //SSH設定タブがクリックされた場合
         $active_ssh_setting = ( isset( $_GET['tabclicked'] ) && 'ssh_setting' == $_GET['tabclicked'] ) ? true : false;
         //設定タブがクリックされた場合
         $active_setting = ( isset( $_GET['tabclicked'] ) && 'setting' == $_GET['tabclicked'] ) ? true : false;
         //情報タブがクリックされた場合
         $active_information = ( isset( $_GET['tabclicked'] ) && 'information' == $_GET['tabclicked'] ) ? true : false;
         //タブがクリックされていない場合
         $active_thisplugin = ($active_backup
                               || $active_restore
                               || $active_transfer
                               || $active_scheduler
                               || $active_ssh_setting
                               || $active_setting
                               || $active_information
                               ) ? false : true;
         $isEnableStatusInfo = $active_thisplugin ||$active_scheduler ||$active_transfer ||$active_setting ? 'none': 'block';
      ?>

      <div id="message_backup_complete">
        <p></p>
      </div>

      <div class="wrap">
        <div id="icon-options-general" class="icon32"><br /></div>
          <h1><?php _e('EASY! Backup & Restore', CONST_SIMPLE_BACKUP_RESTORE_DOMAIN);?></h1>
           <h2 class="nav-tab-wrapper">
           <a href="<?php echo admin_url( 'admin.php?page='.CONST_SIMPLE_BACKUP_RESTORE_ID ); ?>" class="nav-tab <?php if ( $active_thisplugin ) echo ' nav-tab-active'; ?>">
             <?php _e('About this plugin', CONST_SIMPLE_BACKUP_RESTORE_DOMAIN);?></a>
           <a href="<?php echo esc_url( add_query_arg( array('page'=>CONST_SIMPLE_BACKUP_RESTORE_ID, 'tabclicked' => 'backup' ), admin_url( 'admin.php' ) ) ); ?>" class="nav-tab <?php if ( $active_backup ) echo ' nav-tab-active'; ?>">
             <?php _e('Backup', CONST_SIMPLE_BACKUP_RESTORE_DOMAIN);?></a>
           <a href="<?php echo esc_url( add_query_arg( array('page'=>CONST_SIMPLE_BACKUP_RESTORE_ID, 'tabclicked' => 'restore' ), admin_url( 'admin.php' ) ) ); ?>" class="nav-tab <?php if ( $active_restore ) echo ' nav-tab-active'; ?>">
             <?php _e('Restore', CONST_SIMPLE_BACKUP_RESTORE_DOMAIN);?></a>
           <a href="<?php echo esc_url( add_query_arg( array('page'=>CONST_SIMPLE_BACKUP_RESTORE_ID, 'tabclicked' => 'transfer' ), admin_url( 'admin.php' ) ) ); ?>" class="nav-tab <?php if ( $active_transfer ) echo ' nav-tab-active'; ?>">
             <?php _e('Files', CONST_SIMPLE_BACKUP_RESTORE_DOMAIN);?></a>
           <a href="<?php echo esc_url( add_query_arg( array('page'=>CONST_SIMPLE_BACKUP_RESTORE_ID, 'tabclicked' => 'scheduler' ), admin_url( 'admin.php' ) ) ); ?>" class="nav-tab <?php if ( $active_scheduler ) echo ' nav-tab-active'; ?>">
             <?php _e('Scheduler', CONST_SIMPLE_BACKUP_RESTORE_DOMAIN);?></a>
           <a href="<?php echo esc_url( add_query_arg( array('page'=>CONST_SIMPLE_BACKUP_RESTORE_ID, 'tabclicked' => 'ssh_setting' ), admin_url( 'admin.php' ) ) ); ?>" class="nav-tab <?php if ( $active_ssh_setting ) echo ' nav-tab-active'; ?>">
             <?php _e('SSH transfer', CONST_SIMPLE_BACKUP_RESTORE_DOMAIN);?></a>
           <a href="<?php echo esc_url( add_query_arg( array('page'=>CONST_SIMPLE_BACKUP_RESTORE_ID, 'tabclicked' => 'setting' ), admin_url( 'admin.php' ) ) ); ?>" class="nav-tab <?php if ( $active_setting ) echo ' nav-tab-active'; ?>">
             <?php _e('Setting', CONST_SIMPLE_BACKUP_RESTORE_DOMAIN);?></a>
           <a href="<?php echo esc_url( add_query_arg( array('page'=>CONST_SIMPLE_BACKUP_RESTORE_ID, 'tabclicked' => 'information' ), admin_url( 'admin.php' ) ) ); ?>" class="nav-tab <?php if ( $active_information ) echo ' nav-tab-active'; ?>">
             <?php _e('Information', CONST_SIMPLE_BACKUP_RESTORE_DOMAIN);?></a>
           </h2>

  <?php
      if($_SERVER["SERVER_NAME"] != $this->production_domain){
        echo '<br /><b><font color="red">現在'.$_SERVER["SERVER_NAME"].' ('.get_current_user().') 環境で動作しています！！</font></b><br />';
      }
      // Current site prefix
      global $wpdb;
      $restore_tableprefix = @get_option("RESTORE_TABLEPREFIX");
      if(!empty($restore_tableprefix)
        && ($restore_tableprefix != $wpdb->prefix)){
        echo '<br /><b><font color="red">リストアしたテーブルプレフィックスが現在の設定と異なります。'.$wpdb->prefix.'に変更してください！</font></b><br />';
      }
  ?>

        <fieldset style="margin: 20px; display:<?php echo $isEnableStatusInfo; ?>">
          <legend>スクリプトステータス情報&nbsp(<span id="statustime"></span>)&nbsp&nbsp<input type="button" name="ajax_clear" class="button action" style="font-size:x-small;height:18px; border: 0px; padding: 0px; margin:2px;" value="クリア" /></legend>
          <p id="statusmessage"></p>
  <!--        <textarea readonly-->
  <!--                  name="loginfo"-->
  <!--                  type="textarea"-->
  <!--                  id="statusmessage"-->
  <!--                  rows="4"-->
  <!--                  style="width:100%;"-->
  <!--                  >-->
  <!--      </textarea>-->
        </fieldset>
        <hr />

      <?php
      //各タブごとに表示および更新処理が異なるため、選択されたタブにより処理を分岐する
      if($active_thisplugin){
        //このプラグインについて
        //プラグインの説明はabout-this-plugin.htmlを読み込みます
        include_once plugin_dir_path( __FILE__ ).'about-this-plugin.html';
      }else if($active_backup){
        //バックアップ
        $this->showBackupPage();
        if($this->debugMode) $logdata = @file_get_contents(WP_CONTENT_DIR."/backup_sh.log");
      }else if($active_restore){
        //リストア
        $this->showRestorePage();
        if($this->debugMode) $logdata = @file_get_contents(WP_CONTENT_DIR."/restore_sh.log");
      }else if($active_transfer){
        //ファイル操作
        $this->showFileManager();
      }else if($active_scheduler){
        //スケジューラ
        $this->showSchedulerPage();
        if($this->debugMode) $logdata = @file_get_contents(WP_CONTENT_DIR."/scheduler_sh.log");
      }else if($active_ssh_setting){
        //SSH設定
        $this->showSSHSettingPage();
        if($this->debugMode) $logdata = @file_get_contents(WP_CONTENT_DIR."/ssh_transfer_sh.log");
      }else if($active_setting){
        //設定
        $this->showSettingPage();
      }else if($active_information){
        //設定
        $this->showInformationPage();
      }
      ?>

  <?php
      if($this->debugMode){
        ?>
        <fieldset style="display:<?php echo $active_thisplugin ||$active_transfer ||$active_setting ? 'none': 'block'?>">
          <legend>ログ情報</legend>
          <textarea readonly
                    name="loginfo"
                    type="textarea"
                    id="loginfo1_id"
                    rows="8"
                    style="width:100%;"
                    class="regular-text" title="test">
            <?php echo $logdata; ?>
          </textarea>
        </fieldset>
      <?php
      }
    }


    /*****************************************************************************
     * バックアップページの表示と登録処理
     *
     * 配列データよりINPUTタグを出力、ボタン押下時に登録処理をする
     *
    ******************************************************************************/
    private function showBackupPage() {

      $sortby = ( isset( $_GET['sortby'] )) ? $_GET['sortby'] : 'Date';
      $sortorder = ( isset( $_GET['sortorder'] )) ? $_GET['sortorder'] : 'Descending';

      //POSTで返ってきた値により更新処理をする ここから
      if( isset($_POST['action'])
        && ($_POST['action'] == 'backup')) {

        //バックアップディレクトリを取得する
        $backupdir = get_option('backup_srcdirname');
        //メモが入力されていれば一時ファイルに出力する
        if (isset($_POST['memo']) && strlen($_POST['memo']) > 0) {
          $contents = $_POST['memo'];
        }
        $fname_info = "temp_" . str_replace('${SYSDATE}', '', get_option('backup_fnameinfodata'));
        //ファイル名・メモ内容が存在する場合のみメモ内容をスクリプトに引き渡すファイルを生成。
        //※このファイルがなければシェルスクリプト側で自動的にメモを出力する
        if (strlen($fname_info) > strlen("temp_")
          && isset($contents)){
          file_put_contents(ABSPATH . "/" . $backupdir . "/" . $fname_info, $contents . "\n");
        }

        // Current site prefix
        global $wpdb;
        update_option("BACKUP_TABLEPREFIX", $wpdb->prefix);

        //$exec_cmd= WP_CONTENT_DIR . "/started.sh > /dev/null &";
        //exec($exec_cmd);
        $output = array();
        $ret = null;
        $exec_cmd = $this->pre_command_sudo . WP_CONTENT_DIR . "/backup.sh " . WP_CONTENT_DIR . " > " . WP_CONTENT_DIR . "/backup_sh.log &";
        exec($exec_cmd, $output, $ret);


        if ($ret) {
          $mess = "Backup script has been failed.";
        } else {
          $mess = "Backup script has been started.";
        }
        //更新した場合はメッセージを表示する
        $backupedflg = false;
        if ($backupedflg == false) {

          $backupedflg = true;
          $mess = "$mess ($exec_cmd)";
          echo '<div class="updated fade">';
          echo '<p><strong>';
          _e($mess);
          echo '</strong></p></div>';
        }
      }

      //バックアップのタブが選択された場合の処理
      $eeSFL_Titles = array();
      $eeSFL_Files = array();
      $errcode = $this->myPluginFileUtil->getFileList($eeSFL_Files,$eeSFL_Titles, $eeSFL_Errors, get_option('backup_srcdirname'));
      if($errcode){
        $filelist_html = '<p>File Error</p>';
      }else{
        $filelist_html = $this->myPluginFileUtil->makeFileListHtml($eeSFL_Files, 'backup',$sortby, $sortorder, get_option('backup_srcdirname'), false, false);
      }
      ?>

      <h2>バックアップ</h2>
      <p>「バックアップを開始する」のボタンをクリックすると、バックアップ処理が開始されます。</p>
      <?php //TODO: Action見直し
      ?>
      <form action="/wp-admin/admin.php?page=simple-backup-and-restore-plugin&tabclicked=backup" method="post" onsubmit="return checkbeforeexec('バックアップ');">
        <?php wp_nonce_field('shoptions'); ?>
        <fieldset>
          <legend>バックアップ情報</legend>
          メモ
          <input name="memo"
          type="text"
          id="id_memo"
          value=""
          size="32"
          maxlength="256"
          class="regular-text" />
          <p class="submit"><button type='submit' name='action' value='backup' class="button action" >バックアップを開始する</button></p>
        </fieldset>

        <?php echo $filelist_html; ?>

      </form>

  <?php
    }

    /*****************************************************************************
     * リストアページの表示と登録処理
     *
     * 配列データよりINPUTタグを出力、ボタン押下時に登録処理をする
     *
    ******************************************************************************/
    private function showRestorePage() {
      $sortby = ( isset( $_GET['sortby'] )) ? $_GET['sortby'] : 'Date';
      $sortorder = ( isset( $_GET['sortorder'] )) ? $_GET['sortorder'] : 'Descending';

      //localタブがクリックされた場合
      $active_local = ( isset( $_GET['restore_tabclicked'] ) && 'local' == $_GET['restore_tabclicked'] ) ? true : false;
      //serverタブがクリックされた場合
      $active_server = ( isset( $_GET['restore_tabclicked'] ) && 'server' == $_GET['restore_tabclicked'] ) ? true : false;
      if(!$active_local && !$active_server){
        $active_local = true;
      }
      if($active_local) {
        $selectedFolder = get_option('backup_srcdirname');
      }else{
        $selectedFolder = get_option('backup_recieveddirname');
      }

      if( isset($_POST['action'])
        && ($_POST['action'] == 'restore')) {

        $eeSFL_Msg='';
        $prefixCheckOKFlag = true;
        //リストアの前にバックアップフォルダから対象ファイルをリストアフォルダに転送する
        if(isset($_POST['selected-file'] )){
          //テーブルプレフィックスが格納されている場合はチェックする
          $eeSFL_File = $_POST['selected-file'];
          $selecteddata_array = explode(";",$eeSFL_File);
          if(count($selecteddata_array) == 3){
            //テーブルプレフィックスを取得する
            $tableprefix = trim($selecteddata_array[2]);
            //テーブルプレフィックスが一致しない場合はエラーを表示する
            global $wpdb;
            if($tableprefix != $wpdb->prefix){
              $prefixCheckOKFlag = false;
              $mess = "Current table prefix is different form one in backup data!! Please check table prefix.";
            }
          }

          //テーブルプレフィックスが一致する場合はリストア処理を実行する
          if($prefixCheckOKFlag == true){
            //foreach($_POST['target-files'] as $eeSFL_Key => $eeSFL_File){
            $eeSFL_File = trim($selecteddata_array[0]);
            if($this->copyBackupFiletoRestoreFolder(0, $eeSFL_File , $selectedFolder, get_option('backup_destdirname'), $eeSFL_Msg)) {
              //$eeSFL_Msg = __('Copied the file', 'ee-simple-file-list') . ' &rarr; ' . $eeSFL_File;
              $eeLog[] = $eeSFL_Msg;
              $eeSFL_Messages[] = $eeSFL_Msg;

              //リストアフォルダに転送されたファイルをリストアする
              $errmsg="";
              if($this->isAvalableFile($errmsg)){
                //対象ファイルがないのでエラーを表示する
                $mess = "Missing backup files ($errmsg)";
              }
              else {
                //最終リストアファイルの名前を保存する
                update_option('backup_last_restore_file', $eeSFL_File);
                //リストア処理を実行する
                $output = array();
                $ret = null;
                $exec_cmd= $this->pre_command_sudo . WP_CONTENT_DIR."/restore.sh ".WP_CONTENT_DIR." ".CONST_SIMPLE_BACKUP_RESTORE_ID." $eeSFL_File > ".WP_CONTENT_DIR."/restore_sh.log &";
                exec($exec_cmd, $output, $ret);

                if ( $ret ) {
                  $mess="Restore script has been failed.";
                } else {
                  $mess="Restore script has been started.";
                }
                $mess = "$mess ($exec_cmd)";
              }
            } else {
              //$eeSFL_Msg = __('File Copy Failed', 'ee-simple-file-list') . ':' . $eeSFL_File;
              $eeLog[] = $eeSFL_Msg;
              $eeSFL_Messages[] = $eeSFL_Msg;
            }
          }
        }else{
          $mess = 'Restore file is not selected!';
        }




        //更新した場合はメッセージを表示する
        echo '<div class="updated fade">';
        echo '<p><strong>';
        _e($eeSFL_Msg);
        _e($mess);
        echo '</strong></p></div>';

      }


        //リストアのタブが選択された場合の処理
        $errcode = $this->myPluginFileUtil->getFileList($eeSFL_Files,$eeSFL_Titles, $eeSFL_Errors, $selectedFolder);
        if($errcode){
          $filelist_html = '<p>File Error</p>';
        }else{
          $filelist_html = $this->myPluginFileUtil->makeFileListHtml($eeSFL_Files, 'restore',$sortby, $sortorder, $selectedFolder, true, false);
        }
        ?>

        <form action="" method="post" onsubmit="return checkbeforeexec('リストア');">
          <?php wp_nonce_field('shoptions'); ?>
          <h2>リストア</h2>
          <p>バックアップデータの中からリストアしたいデータを選択してください。その後、「リストアを開始する」のボタンをクリックすると、リストア処理が開始されます。</p>

          <h2 class="nav-tab-wrapper">
            <a href="<?php echo esc_url( add_query_arg( array('page'=>CONST_SIMPLE_BACKUP_RESTORE_ID, 'tabclicked' => 'restore', 'restore_tabclicked' => 'local'), admin_url( 'admin.php' ) ) ); ?>" class="nav-tab <?php if ( $active_local ) echo ' nav-tab-active'; ?>">ローカル</a>
            <a href="<?php echo esc_url( add_query_arg( array('page'=>CONST_SIMPLE_BACKUP_RESTORE_ID, 'tabclicked' => 'restore', 'restore_tabclicked' => 'server'), admin_url( 'admin.php' ) ) ); ?>" class="nav-tab <?php if ( $active_server ) echo ' nav-tab-active'; ?>">サーバー受信</a>
          </h2>

          <p class="submit"><button type='submit' name='action' value='restore' class="button action" >リストアを開始する</button></p>
          <?php echo $filelist_html; ?>
        </form>

      <?php
    }

    /*****************************************************************************
     * ファイル操作ページの表示と登録処理
     *
     * 配列データよりINPUTタグを出力、ボタン押下時に登録処理をする
     *
    ******************************************************************************/
    private function showFileManager() {
      $sortby = ( isset( $_GET['sortby'] )) ? $_GET['sortby'] : 'Date';
      $sortorder = ( isset( $_GET['sortorder'] )) ? $_GET['sortorder'] : 'Descending';

      //localタブがクリックされた場合
      $active_local = ( isset( $_GET['restore_tabclicked'] ) && 'local' == $_GET['restore_tabclicked'] ) ? true : false;
      //serverタブがクリックされた場合
      $active_server = ( isset( $_GET['restore_tabclicked'] ) && 'server' == $_GET['restore_tabclicked'] ) ? true : false;
      //設定タブがクリックされた場合
      $active_file_setting = ( isset( $_GET['restore_tabclicked'] ) && 'file_setting' == $_GET['restore_tabclicked'] ) ? true : false;
      if(!$active_local && !$active_server && !$active_file_setting){
        $active_local = true;
      }
      if($active_local) {
        $selectedFolder = get_option('backup_srcdirname');
      }elseif($active_server) {
        $selectedFolder = get_option('backup_recieveddirname');
      }else{
        $selectedFolder = get_option('backup_recieveddirname');
      }
      $trashBoxFolder = get_option('backup_trashboxdirname');

      if( isset($_POST['action'])
        && ($_POST['action'] == 'move_to_trash_box')) {

        //if(isset($_POST['selected-file'] )){
        $eeSFL_Msg='';
        //$eeSFL_File = $_POST['selected-file'];
        foreach($_POST['target-files'] as $eeSFL_Key => $eeSFL_File){
          if($this->copyBackupFiletoRestoreFolder(1, $eeSFL_File ,$selectedFolder, $trashBoxFolder, $eeSFL_Msg)) {
            //$eeSFL_Msg = __('Copied the file', 'ee-simple-file-list') . ' &rarr; ' . $eeSFL_File;
            $eeLog[] = $eeSFL_Msg;
            $eeSFL_Messages[] = $eeSFL_Msg;
          } else {
            //$eeSFL_Msg = __('File Copy Failed', 'ee-simple-file-list') . ':' . $eeSFL_File;
            $eeLog[] = $eeSFL_Msg;
            $eeSFL_Messages[] = $eeSFL_Msg;
          }
          //}
        }
        //更新した場合はメッセージを表示する
        echo '<div class="updated fade">';
        echo '<p><strong>';
        _e($eeSFL_Msg);
        echo '</strong></p></div>';

        // If Copy Files...
      }elseif( isset($_POST['action'])
        && ($_POST['action'] == 'reverse_from_trash_box')) {

        if(isset($_POST['selected-file'] )){
          $eeSFL_Msg='';
          $eeSFL_File = $_POST['selected-file'];
          //foreach($_POST['target-files'] as $eeSFL_Key => $eeSFL_File){
          if($this->copyBackupFiletoRestoreFolder(1, $eeSFL_File, $trashBoxFolder, $selectedFolder , $eeSFL_Msg)) {
            //$eeSFL_Msg = __('Copied the file', 'ee-simple-file-list') . ' &rarr; ' . $eeSFL_File;
            $eeLog[] = $eeSFL_Msg;
            $eeSFL_Messages[] = $eeSFL_Msg;
          } else {
            //$eeSFL_Msg = __('File Copy Failed', 'ee-simple-file-list') . ':' . $eeSFL_File;
            $eeLog[] = $eeSFL_Msg;
            $eeSFL_Messages[] = $eeSFL_Msg;
          }
        }else{
          $eeSFL_Msg = 'file in trash box is not selected!';
        }
        //更新した場合はメッセージを表示する
        echo '<div class="updated fade">';
        echo '<p><strong>';
        _e($eeSFL_Msg);
        echo '</strong></p></div>';

        // If Copy Files...
      }elseif( isset($_POST['action'])
        && ($_POST['action'] == 'clear_trash_box')) {

        $eeSFL_Msg='';
        if($this->clearTrashBoxFolder($trashBoxFolder)) {
          $eeSFL_Msg = __('Cleared trash-box folder successfully.', 'ee-simple-file-list');
          $eeLog[] = $eeSFL_Msg;
          $eeSFL_Messages[] = $eeSFL_Msg;
        } else {
          $eeSFL_Msg = __('Failed to clear trash-box folder.', 'ee-simple-file-list');
          $eeLog[] = $eeSFL_Msg;
          $eeSFL_Messages[] = $eeSFL_Msg;
        }

        //更新した場合はメッセージを表示する
        echo '<div class="updated fade">';
        echo '<p><strong>';
        _e($eeSFL_Msg);
        echo '</strong></p></div>';

      }

        $errcode1 = $this->myPluginFileUtil->getFileList($eeSFL_Files1,$eeSFL_Titles, $eeSFL_Errors, $selectedFolder);
        $errcode2 = $this->myPluginFileUtil->getFileList($eeSFL_Files2,$eeSFL_Titles, $eeSFL_Errors, $trashBoxFolder);
        if($errcode1 || $errcode2){
          $filelist_html = '<p>File Error</p>';
        }elseif($active_local || $active_server){
          $filelist_html = '';
          $filelist_html .= '<table>';
          $filelist_html .= '<tr>';
          $filelist_html .= '<td valign="top">';
          $filelist_html .= $this->myPluginFileUtil->makeFileListHtml($eeSFL_Files1, 'transfer',$sortby, $sortorder, $selectedFolder, false, true);
          $filelist_html .= '<p class="submit"><button type="submit" name="action" value="move_to_trash_box" class="button action" >ゴミ箱にすてる</button></p>';
          $filelist_html .= '</td>';
          $filelist_html .= '<td valign="top">';
          $filelist_html .= $this->myPluginFileUtil->makeFileListHtml($eeSFL_Files2, 'transfer',$sortby, $sortorder, $trashBoxFolder, true, false);
          $filelist_html .= '<p class="submit"><button type="submit" name="action" value="reverse_from_trash_box" class="button action" >ゴミ箱から戻す</button></p>';
          $filelist_html .= '</td>';
          $filelist_html .= '</tr>';
          $filelist_html .= '</table>';
        }elseif($active_file_setting){

          $filelist_html = '';
          $filelist_html .= $this->myPluginFileUtil->makeFileListHtml($eeSFL_Files2, 'transfer',$sortby, $sortorder, $trashBoxFolder, false, false);

        }?>

        <h2>ファイル操作</h2>

      <h2 class="nav-tab-wrapper">
        <a href="<?php echo esc_url( add_query_arg( array('page'=>CONST_SIMPLE_BACKUP_RESTORE_ID, 'tabclicked' => 'transfer', 'restore_tabclicked' => 'local'), admin_url( 'admin.php' ) ) ); ?>" class="nav-tab <?php if ( $active_local ) echo ' nav-tab-active'; ?>">ローカル</a>
        <a href="<?php echo esc_url( add_query_arg( array('page'=>CONST_SIMPLE_BACKUP_RESTORE_ID, 'tabclicked' => 'transfer', 'restore_tabclicked' => 'server'), admin_url( 'admin.php' ) ) ); ?>" class="nav-tab <?php if ( $active_server ) echo ' nav-tab-active'; ?>">サーバー受信</a>
        <a href="<?php echo esc_url( add_query_arg( array('page'=>CONST_SIMPLE_BACKUP_RESTORE_ID, 'tabclicked' => 'transfer', 'restore_tabclicked' => 'file_setting'), admin_url( 'admin.php' ) ) ); ?>" class="nav-tab <?php if ( $active_file_setting ) echo ' nav-tab-active'; ?>">その他</a>
      </h2>

      <form action="" method="post" onsubmit="return checkbeforeexec('ファイルの操作');">
  <?php
          if($active_file_setting) {
            echo '<hr />';
            echo '<h2>ゴミ箱の整理</h2>';
            echo '<hr />';
            echo '<p>ゴミ箱が一杯になった場合は、定期的にゴミ箱を整理してください。<br>';
            echo '次の「ゴミ箱を空にする」ボタンをクリックすると、ゴミ箱の中のファイルがすべて削除*されます。<br>';
            echo '<small>※ゴミ箱の中のファイルは一度削除すると元に戻りませんのでご注意ください。</small></p>';
            echo '<p class="submit"><button type="submit" name="action" value="clear_trash_box" class="button action" >ゴミ箱を空にする</button></p>';
            echo '<hr />';
            echo $filelist_html;
          }else{
            echo '<hr />';
            echo "<h2>${selectedFolder} フォルダの整理</h2>";
            echo '<hr />';
            wp_nonce_field('shoptions');
            echo $filelist_html;
          }
  ?>
        </form>

  <?php
    }


    /*****************************************************************************
     * スケジューラ-ページの表示と登録処理
     *
     * 配列データよりINPUTタグを出力、ボタン押下時に登録処理をする
     *
    ******************************************************************************/
    private function showSchedulerPage() {

      //現在の動作モードを表示する
      $cur_scd_mode = get_option('backup_scd_mode');

      //毎時タブがクリックされた場合
      $active_scd_everyhour = ( isset( $_GET['scd_tabclicked'] ) && 'everyhour' == $_GET['scd_tabclicked'] ) ? true : false;
      //毎日タブがクリックされた場合
      $active_scd_everyday = ( isset( $_GET['scd_tabclicked'] ) && 'everyday' == $_GET['scd_tabclicked'] ) ? true : false;
      //毎週タブがクリックされた場合
      $active_scd_everyweek = ( isset( $_GET['scd_tabclicked'] ) && 'everyweek' == $_GET['scd_tabclicked'] ) ? true : false;
      //毎月タブがクリックされた場合
      $active_scd_everymonth = ( isset( $_GET['scd_tabclicked'] ) && 'everymonth' == $_GET['scd_tabclicked'] ) ? true : false;

      //スケジューラタブが選択されたとき、現在の動作モードをデフォルトのタブとする
      if(!$active_scd_everyhour
        && !$active_scd_everyday
        && !$active_scd_everyweek
        && !$active_scd_everymonth){

        if($cur_scd_mode == 0){
          //nothing
        }elseif($cur_scd_mode == 1){
          $active_scd_everyhour = true;
        }elseif($cur_scd_mode == 2){
          $active_scd_everyday = true;
        }elseif($cur_scd_mode == 3){
          $active_scd_everyweek = true;
        }elseif($cur_scd_mode == 4){
          $active_scd_everymonth = true;
        }
      }

      if($active_scd_everyhour){
        $scd_mode = 1;
        $ary = self::scd_houritemary;
      }else if($active_scd_everyday){
        $scd_mode = 2;
        $ary = self::scd_dayitemary;
      }else if($active_scd_everyweek){
        $scd_mode = 3;
        $ary = self::scd_weekitemary;
      }else if($active_scd_everymonth){
        $scd_mode = 4;
        $ary = self::scd_monthitemary;
      }else{
        $scd_mode = 0;
      }


      //POSTで受け取った値により更新処理を行う
      if(isset($_POST['action'])
        && ($_POST['action'] == 'schedule_off')){
        update_option('backup_scd_minute','*');
        update_option('backup_scd_hour','*');
        update_option('backup_scd_day','*');
        update_option('backup_scd_date','*');
        update_option('backup_scd_target','2099-12-31 23:59');
        //バックアップモードを保存する
        update_option('backup_scd_mode',0);
        $mess= "<p><b>自動バックアップを解除しました</b></p>";
        echo '<div class="updated fade">';
        echo '<p><strong>';
        _e($mess);
        echo '</strong></p></div>';

      }elseif(isset($_POST['action'])
        && $_POST['action'] == 'saved_schedule') {
        //$_POST[$key])があったら保存
        $this->CommonModule_LoadScripts->updateInputTags($ary);

        //デフォルトのメッセージ
        $mess ='Options have been saved.';
        $savedflg=true;

        //スケジューラのタブが選択されている場合は自動バックアップモードを変更する
        if( isset($_POST['selected_scd_tab'])
          && ($_POST['selected_scd_tab'] == '0')
          || $_POST['selected_scd_tab'] == '1'
          || $_POST['selected_scd_tab'] == '2'
          || $_POST['selected_scd_tab'] == '3'
          || $_POST['selected_scd_tab'] == '4') {
          //バックアップモードを保存する
          update_option('backup_scd_mode',intval($_POST['selected_scd_tab']));
          if($_POST['selected_scd_tab'] == '0'){
            update_option('backup_scd_minute','*');
            update_option('backup_scd_hour','*');
            update_option('backup_scd_day','*');
            update_option('backup_scd_date','*');
            $mess= "<p><b>変更後の動作モード：</b>自動バックアップをしない</p>";
          }elseif($_POST['selected_scd_tab'] == '1'){
            update_option('backup_scd_hour','*');
            update_option('backup_scd_day','*');
            update_option('backup_scd_date','*');
            $mess= "<p><b>変更後の動作モード：</b>一時間ごと自動バックアップをする</p>";
          }elseif($_POST['selected_scd_tab'] == '2'){
            update_option('backup_scd_day','*');
            update_option('backup_scd_date','*');
            $mess= "<p><b>変更後の動作モード：</b>毎日自動バックアップをする</p>";
          }elseif($_POST['selected_scd_tab'] == '3'){
            update_option('backup_scd_date','*');
            $mess= "<p><b>変更後の動作モード：</b>毎週自動バックアップをする</p>";
          }elseif($_POST['selected_scd_tab'] == '4'){
            update_option('backup_scd_day','*');
            $mess= "<p><b>変更後の動作モード：</b>毎月自動バックアップをする</p>";
          }
        }

        $output = array();
        $ret = null;
        $mode = 1;  //Backup_scd_targetに次回起動日時を設定
        $exec_cmd= $this->pre_command_sudo .WP_CONTENT_DIR."/scheduler.sh ".WP_CONTENT_DIR." $mode > ".WP_CONTENT_DIR."/scheduler_sh.log &";
        exec($exec_cmd, $output, $ret);
        if($ret == 0){
          $mess .= "Schedule options have been saved.";
        }else{
          $mess = "Schedule options failed to be saved.";
        }

        //更新した場合はメッセージを表示する
        if($savedflg){
          echo '<div class="updated fade">';
          echo '<p><strong>';
          _e($mess);
          echo '</strong></p></div>';
        }
      }
  ?>
      <h2>スケジューラ</h2>


  <?php
      if($cur_scd_mode == 0){
        echo "<p><b>現在の動作モード：</b>自動バックアップをしない</p>";
      }elseif($cur_scd_mode == 1){
        echo "<p><b>現在の動作モード：</b>一時間ごと自動バックアップをする</p>";
      }elseif($cur_scd_mode == 2){
        echo "<p><b>現在の動作モード：</b>毎日自動バックアップをする</p>";
      }elseif($cur_scd_mode == 3){
        echo "<p><b>現在の動作モード：</b>毎週自動バックアップをする</p>";
      }elseif($cur_scd_mode == 4){
        echo "<p><b>現在の動作モード：</b>毎月自動バックアップをする</p>";
      }
   ?>

       <h2 class="nav-tab-wrapper">
       <a href="<?php echo esc_url( add_query_arg( array('page'=>CONST_SIMPLE_BACKUP_RESTORE_ID, 'tabclicked' => 'scheduler', 'scd_tabclicked' => 'everyhour'), admin_url( 'admin.php' ) ) ); ?>" class="nav-tab <?php if ( $active_scd_everyhour ) echo ' nav-tab-active'; ?>">毎時<?php echo ($cur_scd_mode == 1 ? '*':'') ?></a>
       <a href="<?php echo esc_url( add_query_arg( array('page'=>CONST_SIMPLE_BACKUP_RESTORE_ID, 'tabclicked' => 'scheduler', 'scd_tabclicked' => 'everyday'), admin_url( 'admin.php' ) ) ); ?>" class="nav-tab <?php if ( $active_scd_everyday ) echo ' nav-tab-active'; ?>">毎日<?php echo ($cur_scd_mode == 2 ? '*':'') ?></a>
       <a href="<?php echo esc_url( add_query_arg( array('page'=>CONST_SIMPLE_BACKUP_RESTORE_ID, 'tabclicked' => 'scheduler', 'scd_tabclicked' => 'everyweek'), admin_url( 'admin.php' ) ) ); ?>" class="nav-tab <?php if ( $active_scd_everyweek ) echo ' nav-tab-active'; ?>">毎週<?php echo ($cur_scd_mode == 3 ? '*':'') ?></a>
       <a href="<?php echo esc_url( add_query_arg( array('page'=>CONST_SIMPLE_BACKUP_RESTORE_ID, 'tabclicked' => 'scheduler', 'scd_tabclicked' => 'everymonth'), admin_url( 'admin.php' ) ) ); ?>" class="nav-tab <?php if ( $active_scd_everymonth ) echo ' nav-tab-active'; ?>">毎月<?php echo ($cur_scd_mode == 4 ? '*':'') ?></a>
       </h2>

      <div class="wrap">
        <div id="icon-options-general" class="icon32"><br /></div>
        <form action="" method="post">
          <?php

          if($scd_mode > 0) {

            if ($cur_scd_mode == $scd_mode
              && $cur_scd_mode > 0) {
              echo "<p><b><font color='red'>現在この設定で自動バックアップが有効になっています</font></b></p>";
            }

            wp_nonce_field('shoptions');
            //選択されたバックアップ周期タブにより異なる入力項目を表示する
            $this->CommonModule_LoadScripts->showInputTags($ary);
  ?>
            <input name="selected_scd_tab"
                 type="hidden"
                 id="selected_scd_tab01"
                 value="<?php echo $scd_mode; ?>" />
          <p class="submit"><button  class="button-primary" type='submit' name='action' value='saved_schedule'>変更を保存</button></p>
          <p class="submit"><button type="submit" name="action" value="schedule_off" class="button action" >自動バックアップを解除する</button></p>
  <?php
          }
          else {
            //自動バックアップが解除されている場合は以下を表示する
            echo "<p>自動バックアップは現在設定されていません。</p>";
            echo "<p>※設定する場合は上のバックアップ周期のタブを選択して、日時を設定してください。</p>";
          }
          ?>
        </form>

        <!-- /.wrap --></div>
      <?php
    }


    /*****************************************************************************
     * SSH設定ページの表示と登録処理
     *
     * 配列データよりINPUTタグを出力、ボタン押下時に登録処理をする
     *
    ******************************************************************************/
    private function showSSHSettingPage() {
      $ary = self::ssh_itemary;

      if(isset($_POST['action'])
        && $_POST['action'] == 'saved_ssh_setting') {
        //$_POST[$key])があったら保存
        $this->CommonModule_LoadScripts->updateInputTags($ary);

        //デフォルトのメッセージ
        $mess = 'Options have been saved.';
        $savedflg = true;

        //更新した場合はメッセージを表示する
        if ($savedflg) {
          echo '<div class="updated fade">';
          echo '<p><strong>';
          _e($mess);
          echo '</strong></p></div>';
        }

      }elseif(isset($_POST['action'])
          && ($_POST['action'] == 'exec_ssh_transfer')) {

        //サーバー最新データ受信処理をする
        $output = array();
        $ret = null;
        $exec_cmd= $this->pre_command_sudo .WP_CONTENT_DIR."/ssh_transfer.sh ".WP_CONTENT_DIR." > ".WP_CONTENT_DIR."/ssh_transfer_sh.log &";
        exec($exec_cmd, $output, $ret);

        if ($ret) {
          $mess = "SSH transfer script failed.";
        } else {
          $mess = "SSH transfer script has been started.";
        }
        //更新した場合はメッセージを表示する
        $backupedflg = false;
        if ($backupedflg == false) {

          $backupedflg = true;
          $mess = "$mess ($exec_cmd)";
          echo '<div class="updated fade">';
          echo '<p><strong>';
          _e($mess);
          echo '</strong></p></div>';
        }
      }
      ?>
      <h2>SSH設定</h2>
      <div class="wrap">
        <div id="icon-options-general" class="icon32"><br /></div>
        <form action="" method="post">
          <?php
          wp_nonce_field('shoptions');
          $this->CommonModule_LoadScripts->showInputTags(self::ssh_itemary);
          ?>
          <p class="submit"><button  class="button-primary" type='submit' name='action' value='saved_ssh_setting'>変更を保存</button></p>
        </form>

        <form action="" method="post" onsubmit="return checkbeforeexec('SSH転送');">
        <p class="submit"><button type="submit" name="action" value="exec_ssh_transfer" class="button action" >サーバーから最新データの受信を開始する</button></p>
        </form>

  <!--      <form action="" method="post" onsubmit="return checkbeforeexec('受信データのリストア');">-->
  <!--      <p class="submit"><button type="submit" name="action" value="ssh_restore" class="button action" >受信済み最新データのリストアを開始する</button></p>-->
  <!--      </form>-->

        <!-- /.wrap --></div>
      <?php
    }

    /*****************************************************************************
     * 設定ページの表示と登録処理
     *
     * 配列データよりINPUTタグを出力、ボタン押下時に登録処理をする
     *
    ******************************************************************************/
    private function showSettingPage() {
      $ary = self::itemary;

      //DBオプションのAdditionalScriptFileが空ならWP-CONTENTに出力済みのバックアップ用テキストファイルの内容で更新する
      $ret = $this->setWPOptionfromAdditionalScriptFile();

      if( isset($_POST['action'])
        && $_POST['action'] == 'saved_setting') {
        //$_POST[$key])があったら保存
        $this->CommonModule_LoadScripts->updateInputTags($ary);
        //追加スクリプトが入力されていればスクリプトファイルをWP_CONTENTSフォルダに作成する
        $this->createAdditionalScriptFile(true);

        //デフォルトのメッセージ
        $mess ='Options have been saved.';
        $savedflg=true;

        //更新した場合はメッセージを表示する
        if($savedflg){
          echo '<div class="updated fade">';
          echo '<p><strong>';
          _e($mess);
          echo '</strong></p></div>';
        }

      }elseif( isset($_POST['action'])
        && ($_POST['action'] == 'copy_script')) {

        $ret = $this->copyBackupRestoreShellScript();
        if ($ret) {
          $mess = "Script has been copied to wp-content folder successfully.";
        } else {
          $mess = "Script failed to be copied to wp-content folder.";
        }
        //更新した場合はメッセージを表示する
        echo '<div class="updated fade">';
        echo '<p><strong>';
        _e($mess);
        echo '</strong></p></div>';
      }

      ?>
      <h2>設定</h2>
      <div class="wrap">
        <div id="icon-options-general" class="icon32"><br /></div>
        <form action="" method="post">
          <p class="submit"><button  class="button-primary" type='submit' name='action' value='saved_setting'>変更を保存</button></p>
          <?php
          wp_nonce_field('shoptions');
          $this->CommonModule_LoadScripts->showInputTags(self::itemary);
          ?>
          <p class="submit"><button  class="button-primary" type='submit' name='action' value='saved_setting'>変更を保存</button></p>
          <p class="submit"><button type='submit' name='action' value='copy_script'>Copy script *Click this only if backup/restore script does not work*</button></p>
        </form>

        <!-- /.wrap --></div>
      <?php
    }


    /*****************************************************************************
     * 設定ページの表示と登録処理
     *
     * 配列データよりINPUTタグを出力、ボタン押下時に登録処理をする
     *
    ******************************************************************************/
    private function showInformationPage() {

    if(isset($_POST['action'])
      && ($_POST['action'] == 'exec_script_test')) {

          //サーバー最新データ受信処理をする
        $output = array();
        $ret = null;
        $exec_cmd= $this->pre_command_sudo .WP_CONTENT_DIR."/script-test.sh ".WP_CONTENT_DIR." > ".WP_CONTENT_DIR."/script-test_sh.log &";
        exec($exec_cmd, $output, $ret);

        if ($ret) {
        $mess = "TEST script failed.";
        } else {
          $mess = "TEST script has been started.";
        }
        //更新した場合はメッセージを表示する
        $backupedflg = false;
        if ($backupedflg == false) {

            $backupedflg = true;
            $mess = "$mess ($exec_cmd)";
            echo '<div class="updated fade">';
            echo '<p><strong>';
            _e($mess);
            echo '</strong></p></div>';
        }
    }
    if($this->debugMode == "0"){
  ?>
      <h2>スクリプト実行環境テスト</h2>
      <P>テスト用スクリプトを実行して基本設定で登録した内容でスクリプト実行環境を確認することができます</P>
      <form action="" method="post" onsubmit="return checkbeforeexec('スクリプト実行環境テスト');">
        <p class="submit"><button type="submit" name="action" value="exec_script_test" class="button action" >スクリプト実行環境をテストする</button></p>
      </form>
      <hr />
      <?php
    }
  ?>

      <h2>サーバー情報</h2>
      <br />
      <div class="wrap">
        <div id="icon-options-general" class="icon32"><br /></div>

        <h3><?php _e('Resource', CONST_SIMPLE_BACKUP_RESTORE_DOMAIN); ?></h3>
        <?php
        echo $this->getResourceInfo("df -h");
        echo "<br />";
        echo "<br />";
        ?>

        <h3>CRON設定情報</h3>
        <?php
        if($handle = popen('crontab -l 2>&1', 'r')) {
          while (!feof($handle)) {
            $str = fgets($handle);
            echo $str;
            echo "<br />";
          }
          pclose($handle);
        }
        echo "<br />";
        ?>
        <h3>PHPサーバー変数</h3>
          <?php
          echo "SERVER_HOST: ".$_SERVER['HTTP_HOST']."<br />";
          echo "SERVER_NAME: ".$_SERVER['SERVER_NAME']."<br />";
          echo "SERVER_ADR: ".$_SERVER['SERVER_ADDR']."<br />";
          echo "REMOTE_ADR: ".$_SERVER['REMOTE_ADDR']."<br />";
          echo "get_current_user: ". get_current_user() ."<br />";
          echo "<br />";
          echo "<br />";
          ?>
        <h3>Active Plugin情報</h3>
          <?php
          echo "<b>Active Plugins:</b><br> ";
          foreach(get_option( 'active_plugins') as $i => $val){
          echo $i.": ".$val."<br />";
          }
          echo "<br />";
          echo "<br />";
          if($this->debugMode){
        ?>
        <h3>PHP情報（phpinfo）</h3>
          <?php
            phpinfo();
          }
        ?>
        <!-- /.wrap --></div>
      <?php
    }

  /*****************************************************************************
   * リストア対象ファイルのチェック
   *
   * リストア対象ファイルがすべてあるかどうかチェックする
   *
   * @param $errmsg
   * @return int|string
    ******************************************************************************/
    private function isAvalableFile(&$errmsg){
        $errmsg = "";
        $errno = 0;
        $dir_dest = get_option('backup_destdirname');

        $filenames = array();

        $filenames[1] = str_replace('${SYSDATE}','',get_option('backup_fnamemysqldata'));
        $filenames[2] = str_replace('${SYSDATE}','',get_option('backup_fnamewpthemes'));
        $filenames[4] = str_replace('${SYSDATE}','',get_option('backup_fnamewpplugins'));
        $filenames[8] = str_replace('${SYSDATE}','',get_option('backup_fnamewpuploads'));
        foreach ($filenames as $no => $fname) {
          if (! file_exists(ABSPATH."/".$dir_dest."/".$fname)) {
            $errmsg .= $fname . ", ";
            $errno += $no;
          }
        }
        //エラー番号を返す
        return $errno;
    }

  /*****************************************************************************
   * ゴミ箱のファイルをすべて削除する
   *
   * @param $targetdir
   * @return int|string
    ******************************************************************************/
    private function clearTrashBoxFolder($targetdir){

      $ret = true;

      //削除対象のディレクトリを確認する
      if(!isset($targetdir) || strlen($targetdir) == 0){
        return 0;
      }

      //削除対象のファイルのパスを取得する
      $targetfiles = glob(ABSPATH."/".$targetdir."/*");

      foreach ($targetfiles as $file){
        //ファイルを削除する
        if (!unlink($file)){
          $ret = false;
        }
      }
      return($ret);
    }

  /*****************************************************************************
   * 対象ファイルをリストアフォルダに移動
   *
   * 対象ファイルをリストアフォルダに移動する
   * NOTE: DB,WP-THEMES,WP-PLUGIN,WP-UPLOADのファイル名が含まれる場合のみ実行する
   *
  * @param $mode
  * @param $target
  * @param $dir_src
  * @param $dir_dest
  * @param $mess
   * @return bool
   ******************************************************************************/
    private function copyBackupFiletoRestoreFolder($mode, $target,$dir_src,$dir_dest, &$mess){
        $retval = true;
        $filenames = array();
  //      $dir_src = get_option('backup_srcdirname');
  //      $dir_dest = get_option('backup_destdirname');
        $originalFileName = str_replace('${SYSDATE}','',get_option('backup_fnamemysqldata'));

        $filenames[] = array('plain-name' => $originalFileName,'required'=> true);
        $filenames[] = array('plain-name' => str_replace('${SYSDATE}','',get_option('backup_fnamewpthemes')),'required'=>  true);
        $filenames[] = array('plain-name' => str_replace('${SYSDATE}','',get_option('backup_fnamewpplugins')),'required'=>  true);
        $filenames[] = array('plain-name' => str_replace('${SYSDATE}','',get_option('backup_fnamewpuploads')),'required'=>  true);
        $filenames[] = array('plain-name' => str_replace('${SYSDATE}','',get_option('backup_fnameinfodata')),'required'=>  true);
        $filenames[] = array('plain-name' => str_replace('${SYSDATE}','',get_option('backup_fnamewpconfig')),'required'=>  false);

        //$originalFileNameに設定されたファイルの場合次の処理をする
        if(strpos($target, $originalFileName) !== false){
          //$filenamesに設定されたファイルをすべてRESTOREフォルダに転送する
          foreach($filenames as $key => $filename){
            //日付の部分をそのままに基準に設定したファイル名だけを対象のファイル名に置き換える
            $srcfilename = str_replace($originalFileName,$filename['plain-name'], $target);
            if(file_exists(ABSPATH . "/" . $dir_src . "/" . $srcfilename)) {
              $actlabel = array();
              if ($mode == 0) {
                $ret = @copy(ABSPATH . "/" . $dir_src . "/" . $srcfilename, ABSPATH . "/" . $dir_dest . "/" . $filename['plain-name']);
                $actlabel[] = 'copy';
                $actlabel[] = 'copied';
              } else {
                $ret = @rename(ABSPATH . "/" . $dir_src . "/" . $srcfilename, ABSPATH . "/" . $dir_dest . "/" . $srcfilename);
                $actlabel[] = 'move';
                $actlabel[] = 'moved';
              }
              if ($ret) {
                $mess .= $srcfilename . " $actlabel[1] to ".$filename['plain-name']." successfully.<br />";
              } else {
                $mess .= "<font color='red'>$srcfilename failed to $actlabel[0] to ".$filename['plain-name'].".</font><br />";
                $retval = false;
              }
            }else{
              //必須の場合はエラー表示してエラーを返す、そうでない場合はメッセージ表示のみ行う
              if($filename['required']) {
                $mess .= "<font color='red'>$srcfilename is not found.</font><br />";
                $retval = false;
              }else{
                $mess .= "$srcfilename is not found.<br />";
              }
            }
          }
        }
        return $retval;
    }

   /*****************************************************************************
    * リソース情報の取得
    *
    ******************************************************************************/
    private function getResourceInfo($cmd){
      $ret = "";
      $handle = popen("$cmd 2>&1", 'r');
      $ret .= "<h4>$cmd: '$handle'-> " . gettype($handle) . "</h4>";
  //      $read = fread($handle, 2096);
  //      echo nl2br($read);
      $ret .= "<table>";
      while (!feof($handle)) {
        $str = fgets($handle);
        if(strlen($str)<1){
          continue;
        }
        $str2 = trim($str);
        $str2 = preg_replace('/\s+/', ' ', $str2);
        if(strpos($str2,'Mounted on') !== false) {
          $str2 = str_replace('Mounted on', 'Mounted-on', $str2);
          $str2 = "<td><b>" . str_replace(" ", "</b></td><td><b>", $str2) . "</b></td>";
        }else{
          $str2 = "<td>" . str_replace(" ", "</td><td>", $str2) . "</td>";
        }
        //echo "<td>$str</td>";
        $ret .= "<tr>";
        $ret .= "$str2";
        $ret .= "</tr>";
      }
      $ret .= "</table>";
      pclose($handle);
      return $ret;
    }

    /*****************************************************************************
     * DBオプションに登録済みの追加スクリプトデータをファイルに出力する
     * @return bool 成功すればTrue、失敗すればFalseを返す
     *****************************************************************************/
    private function createAdditionalScriptFile($updatemode = false){
      $filepath_save = WP_CONTENT_DIR.'/' . self::ADD_SCRIPT_FILE_SAVED_DBOPTION;
      $filepath_exec = WP_CONTENT_DIR.'/' . self::ADD_SCRIPT_FILE_EXECUTABLE;
      if ($updatemode == false
        && file_exists( $filepath_save )) {
        //対象ファイルが存在する
        return true;
      } else {
        //オプションから登録済みスクリプト読み込み
        $backup_wpcli_alias = @get_option('backup_wpcli_alias');
        $backup_php_path = @get_option('backup_php_path');
        $backup_allow_root = @get_option('backup_allow_root');
        $backup_addtional_script = @get_option('backup_addtional_script');
        if(!empty($backup_addtional_script) && strlen($backup_addtional_script)>0) {
          //不要な円マークを除去する
          $backup_addtional_script = str_replace('\\', '', $backup_addtional_script);
          $backup_addtional_script = preg_replace("/(\r)+/", '', $backup_addtional_script);

          //オプション登録済みスクリプトをファイルに出力する（バックアップ保存用）
          if (file_put_contents($filepath_save, $backup_addtional_script) !== false) {
            @chgrp($filepath_save, "apache");
            @chmod($filepath_save, 0777);
          }

          file_put_contents($filepath_exec, '# Additional script' . PHP_EOL);
          //オプション登録済みスクリプトをファイルに出力する(実行用シェルスクリプト作成)
          if (file_put_contents($filepath_exec, $backup_addtional_script . PHP_EOL) !== false) {
            @chgrp($filepath_exec, "apache");
            @chmod($filepath_exec, 0777);
          }

          file_put_contents($filepath_exec, '# Allow root or not' . PHP_EOL, FILE_APPEND);
          $allow_root = '';
          if(isset($backup_allow_root) && $backup_allow_root == '1') {
            $allow_root = 'ALLOW_ROOT="--allow-root"';
          }else{
            $allow_root = 'ALLOW_ROOT=""';
          }
          file_put_contents($filepath_exec, $allow_root . PHP_EOL, FILE_APPEND);

          file_put_contents($filepath_exec, '# Set alias for WP-CLI script' . PHP_EOL, FILE_APPEND);
          if(isset($backup_wpcli_alias) && $backup_wpcli_alias == '1'){
            // シェルが非対話の場合、shopt を使用して expand_aliases シェルオプションを設定しない限り、エイリアスは展開されません
            // （以下のSHELL BUILTINコマンドの下のshoptの説明を参照）
            // https://genzouw.com/entry/2020/03/16/090918/1947/
            $wp_alias = "shopt -s expand_aliases";
            file_put_contents($filepath_exec,$wp_alias . PHP_EOL, FILE_APPEND);
            //ALIAS FOR PHP
            $wp_alias = "alias php='".$backup_php_path . "'";
            file_put_contents($filepath_exec,$wp_alias . PHP_EOL, FILE_APPEND);
            //ALIAS FOR WP
            $wp_alias = "alias wp='".$backup_php_path . ' ' . WP_CONTENT_DIR . "/wp-cli.phar'";
            file_put_contents($filepath_exec,$wp_alias . PHP_EOL, FILE_APPEND);
          }

          return true;
        }else{
          //対象オプションに何も入っていないので処理しない
          return true;
        }
      }
      //ファイル生成に失敗すればFALSEを返す
      return false;
    }

    /*****************************************************************************
     * 追加スクリプトファイルのデータをDBオプションに出力する
     * @return bool 成功すればTrue、失敗すればFalseを返す
     *****************************************************************************/
    private function setWPOptionfromAdditionalScriptFile(){
      //オプションから登録済みデータを読み込み
      $backup_addtional_script_option = @get_option('backup_addtional_script');
      if(!empty($backup_addtional_script_option) && strlen($backup_addtional_script_option)>0) {
        //DBオプションのaddtional_scriptはデータが存在するので更新しない
        return true;
      }
      //DBオプションのaddtional_scriptはデータをbackup_addtional_script.shで更新する
      $filepath = WP_CONTENT_DIR.'/' . self::ADD_SCRIPT_FILE_SAVED_DBOPTION;
      if (file_exists( $filepath )) {
        //ファイルを読み込む
        $backup_addtional_script = file_get_contents($filepath);
        if ($backup_addtional_script === false) {
          return false;
        }else{
          if(!empty($backup_addtional_script) && strlen($backup_addtional_script)>0) {
            //不要な円マークを除去する
            $backup_addtional_script = str_replace('\\', '', $backup_addtional_script);
            $backup_addtional_script = preg_replace("/(\r)+/", '', $backup_addtional_script);
            //DBオプションに登録
            update_option('backup_addtional_script', $backup_addtional_script);
          }else{
            //ファイルが空の場合、DBオプションにコメントのみ登録
            update_option('backup_addtional_script', '# *** Additional script code area ***');
          }
          //ファイルが空の場合もTrueを返す
          return true;
        }
      }else{
      	  //ファイルが存在しない場合、DBオプションにコメントのみ登録
		  update_option('backup_addtional_script', '# *** Additional script code area ***');
          //ファイルが空の場合もTrueを返す
          return true;
      }
      //ファイルが存在しない場合でもTrueを返す
      return true;
    }
  }
}
