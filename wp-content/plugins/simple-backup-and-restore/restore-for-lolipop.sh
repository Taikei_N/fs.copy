#!/bin/bash
# restore.sh
#
# -----------------------------------------------------------------------------
# Title : リストア処理
# -----------------------------------------------------------------------------
# 2018.10.13 created
#
# Description :
#   backup.shで作成したバックアップファイルからWPのDBとファイルをリストアする
#
# Arguments :
#   [Arg1] WP_CONTENT_DIR
#   [Arg2] 自身のプラグインフォルダ名   
#
# Return value :
#   =0: OK
#   >0: ERROR CODE
#
#
# -----------------------------------------------------------------------------


function restorewpcontent() {
	TEMP_DIR=${ABSPATH}${backup_tempdirname}/

#解凍前にフォルダを削除する
	echo "rm -Rf ${TEMP_DIR}themes"
	rm -rf ${TEMP_DIR}themes
	echo "rm -Rf ${TEMP_DIR}uploads"
	rm -rf ${TEMP_DIR}uploads
	echo "rm -Rf ${TEMP_DIR}plugins"
	rm -rf ${TEMP_DIR}plugins

#バックアップファイルをTEMPフォルダに展開する
	echo "$TARPATH xfvz ${DEST_BACKUP_DIR}${backup_fnamewpthemes2} -C ${TEMP_DIR}"
	$TARPATH xfvz ${DEST_BACKUP_DIR}${backup_fnamewpthemes2} -C ${TEMP_DIR}

	echo "$TARPATH xfvz ${DEST_BACKUP_DIR}${backup_fnamewpplugins2} --exclude plugins/${ARG_MY_PLUGIN_NAME} -C ${TEMP_DIR}"
	$TARPATH xfvz ${DEST_BACKUP_DIR}${backup_fnamewpplugins2} --exclude plugins/${ARG_MY_PLUGIN_NAME} -C ${TEMP_DIR}

	echo "$TARPATH xfvz ${DEST_BACKUP_DIR}${backup_fnamewpuploads2} -C ${TEMP_DIR}"
	$TARPATH xfvz ${DEST_BACKUP_DIR}${backup_fnamewpuploads2} -C ${TEMP_DIR}

#WP_CONTENT以下を削除：※このコメントアウトを外すこと
	echo "rm -rf ${WP_CONTENT_DIR}themes/"
	rm -rf ${WP_CONTENT_DIR}themes/

	echo "rm -rf ${WP_CONTENT_DIR}uploads/"
	rm -rf ${WP_CONTENT_DIR}uploads/

    echo "pushd ${WP_CONTENT_DIR}plugins"
    pushd ${WP_CONTENT_DIR}plugins
	echo "ls ${WP_CONTENT_DIR}plugins/ | grep -v -E ${ARG_MY_PLUGIN_NAME}|index.php | xargs rm -rf"
	ls ${WP_CONTENT_DIR}plugins/ | grep -v -E "${ARG_MY_PLUGIN_NAME}|index.php" | xargs rm -rf
	echo "popd"
	popd

#展開したファイルのWP-CONTENTフォルダをWORDPRESSフォルダに移動する
	echo "cp -rf ${TEMP_DIR}plugins ${WP_CONTENT_DIR}"
	\cp -rf ${TEMP_DIR}plugins ${WP_CONTENT_DIR}
	echo "cp -rf  ${TEMP_DIR}themes ${WP_CONTENT_DIR}"
	\cp -rf ${TEMP_DIR}themes ${WP_CONTENT_DIR}
	echo "cp -rf  ${TEMP_DIR}uploads ${WP_CONTENT_DIR}"
	\cp -rf ${TEMP_DIR}uploads ${WP_CONTENT_DIR}

	###《未使用：上記のcpバージョンに置き換え》TEMP/PLUGINS以下の対象プラグイン以外のフォルダとファイルをすべてWP-CONTENTのPLUGINSに移動する
	###mv ${TEMP_DIR}plugins/* ${WP_CONTENT_DIR}/plugins/
	###pluginsディレクトリが残るので削除する

}

function restoredatabase() {
#データベースをリストアする
	#${MYSQLBIN} -u ${MYSQL_USR} --password=${MYSQL_PWD} -D ${MYSQL_DATABASE} < ${BACKUP_DIR}${SQL_TEMP_FILE}

    current_siteurl=`wp option get siteurl ${ALLOW_ROOT}`    #WP設定のサイトURLを取得する
    echo "リストア前のサイトURLは、${current_siteurl}です。"

	echo データベースのリストアを行う
echo "wp db import ${DEST_BACKUP_DIR}${backup_fnamemysqldata2} ${ALLOW_ROOT}"
	wp db import ${DEST_BACKUP_DIR}${backup_fnamemysqldata2} ${ALLOW_ROOT}

    new_siteurl=`wp option get siteurl ${ALLOW_ROOT}`    #WP設定のサイトURLを取得する
    echo "バックアップデータのサイトURLは、${new_siteurl}です。"

	#最終リストアにファイル名を設定する（リストア直後に最終リストア情報を更新する）
	if [ -n "${ARG_TARGET_FILENAME}" ]; then
		echo "データベースの最終リストア情報を更新する（${ARG_TARGET_FILENAME}）"
		wp option update backup_last_restore_file ${ARG_TARGET_FILENAME} ${ALLOW_ROOT}
	fi
}

function modifyurl(){
#サイトURLを移行先のURLに変更する
	current_siteurl=`wp option get siteurl ${ALLOW_ROOT}` 
	echo "サイトURLを移行先のURL(${current_siteurl}⇒${backup_newsiteurl})に変更する"
	#wp option update siteurl ${backup_newsiteurl} ${ALLOW_ROOT}
	#wp option update home ${backup_newsiteurl} ${ALLOW_ROOT}
	wp search-replace ${current_siteurl} ${backup_newsiteurl} ${ALLOW_ROOT}
}



#################################################
# リストアシェルスクリプト
#
#
#################################################
echo start
# 実行時に指定された引数の数、つまり変数 $# の値が 3 でなければエラー終了。

if [ $# -eq 2 ]; then
    ARG_WP_CONTENT_DIR=$1
    ARG_MY_PLUGIN_NAME=$2
    ARG_TARGET_FILENAME=""
    echo 引数が2個設定されました
elif [ $# -eq 3 ]; then
    ARG_WP_CONTENT_DIR=$1
    ARG_MY_PLUGIN_NAME=$2
    ARG_TARGET_FILENAME=$3
    echo 引数が3個設定されました
else
    echo "引数の数が不正です（第一引数にカレントディレクトリ、第二引数に自身のプラグインが置かれているフォルダ名、第三引数にリストア対象のファイル名を設定する）"
    exit 1
fi


####################################################
# 初期設定
####################################################
cd ${ARG_WP_CONTENT_DIR}

#パスを通す
export PATH=$PATH:/usr/local/bin/

#ROOT権限の場合はこのオプションをつけないとエラーになる
ALLOW_ROOT="--allow-root"
#ルート権限を持たないので不要
ALLOW_ROOT=""
shopt -s expand_aliases
alias wp='/usr/local/php7.1/bin/php /home/users/0/main.jp-kaidukairyou/bin/wp-cli.phar'

#logファイルを作成する
LOG_FILE="backup-process-status.txt"
COMPLETE_FILE="complete.txt"

WP_CONTENT_DIR="${ARG_WP_CONTENT_DIR}"
#最後にスラッシュがあるかどうか判定する
if [ "${ARG_WP_CONTENT_DIR:${#ARG_WP_CONTENT_DIR}-1:1}" != "/" ]; then
	#最後にスラッシュがないのでスラッシュを入れる
	WP_CONTENT_DIR="${WP_CONTENT_DIR}/"
fi

rm -f ${WP_CONTENT_DIR}${LOG_FILE}
touch ${WP_CONTENT_DIR}${LOG_FILE}
chown apache:apache ${WP_CONTENT_DIR}${LOG_FILE}
chmod 777 ${WP_CONTENT_DIR}${LOG_FILE}

echo `date '+%Y%m%d_%H%M%S'`: リストア処理を開始しました >> ${WP_CONTENT_DIR}${LOG_FILE}


#タイムスタンプを取得、変数に設定する
SYSDATE=`date '+%Y%m%d_%H%M%S_'`

#WP設定画面で設定した情報を変数に取得する
source ${WP_CONTENT_DIR}backup_option.sh


#SSH関連情報を変数に取得する
backup_srchomepath=`wp option get backup_srchomepath ${ALLOW_ROOT}`    #転送元ドキュメントルートパス
echo $backup_srchomepath
backup_sshsrchostname=`wp option get backup_sshsrchostname ${ALLOW_ROOT}`    #ssh転送元ホスト名
echo $backup_sshsrchostname
backup_sshsrcportno=`wp option get backup_sshsrcportno ${ALLOW_ROOT}`    #ssh転送元ポートNo
echo $backup_sshsrcportno
backup_sshsrcusername=`wp option get backup_sshsrcusername ${ALLOW_ROOT}`    #ssh転送元ユーザ名
echo $backup_sshsrcusername
backup_sshsrcprivatekeypath=`wp option get backup_sshsrcprivatekeypath ${ALLOW_ROOT}`    #ssh転送元プライベートキーパス
echo $backup_sshsrcprivatekeypath
backup_sshsrcprivatekeyname=`wp option get backup_sshsrcprivatekeyname ${ALLOW_ROOT}`    #ssh転送元プライベートキー名称
echo $backup_sshsrcprivatekeyname
backup_sshnotification=`wp option get backup_sshnotification ${ALLOW_ROOT}`    #ssh経由バックアップ完了通知 1する,0しない
echo $backup_sshnotification
backup_production_dmain=`wp option get backup_production_dmain ${ALLOW_ROOT}`    #本番環境 ドメイン名
echo $backup_production_dmain
backup_debugmode=`wp option get backup_debugmode ${ALLOW_ROOT}`    #デバッグ環境 ログ出力
echo $backup_debugmode
backup_ajaxmode=`wp option get backup_ajaxmode ${ALLOW_ROOT}`    #デバッグ環境 AJAX通信モード
echo $backup_ajaxmode
backup_vagrantmode=`wp option get backup_vagrantmode ${ALLOW_ROOT}`    #デバッグ環境 VAGRANTモード
echo $backup_vagrantmode
backup_scd_execmode=`wp option get backup_scd_execmode ${ALLOW_ROOT}`    #スケジューラ 自動実行モード
echo $backup_scd_execmode


TARPATH="$(which tar)"

#SYSDATE=_`date '+%Y%m%d'`
#SYSDATE=

#SSH_KEY="/home/kusanagi/diy2.smart-agri/ppk/smart-agri-info.key"

#SRC_BACKUP_DIR="/home/kusanagi/diy.smart-agri/backup/"
#DEST_BACKUP_DIR="/home/kusanagi/diy.smart-agri/backup_diy/"
#WP_CONTENT_DIR="/home/kusanagi/diy.smart-agri/DocumentRoot/wp-content/"

#SQL_TEMP_FILE="mysql_diy.smart-agri_backup${SYSDATE}*.sql"
#FILENAME_WP_CONTENT_THEMES="bup_wp-content_themes${SYSDATE}*.tar.gz"
#FILENAME_WP_CONTENT_PLUGINS="bup_wp-content_plugins${SYSDATE}*.tar.gz"
#FILENAME_WP_CONTENT_UPLOADS="bup_wp-content_uploads${SYSDATE}*.tar.gz"
#TEMP_DIR="/home/kusanagi/diy.smart-agri/temp/"
#TEMP_WP_CONTENT_DIR=${TEMP_DIR}"home/kusanagi/diy.smart-agri/DocumentRoot/wp-content/"
#SH_DIR="/home/kusanagi/diy.smart-agri/sh/"

echo `date '+%Y%m%d_%H%M%S'`: "オプションデータの読み込みを完了しました"  >> ${WP_CONTENT_DIR}${LOG_FILE}



#################################################
#WPのバックアップファイルをリストアする
#################################################
#${SH_DIR}restore_wp-content.sh 1
echo "WP-CONTENTを展開中・・・"
restorewpcontent
echo "WP-CONTENTを展開完了しました"

#logファイルを作成する
echo `date '+%Y%m%d_%H%M%S'`: "WPファイルのリストアを完了しました。"  >> ${WP_CONTENT_DIR}${LOG_FILE}

#################################################
#データベースをインポートする
#################################################
#${WP_CONTENT_DIR}restore_database.sh 1
#以下に展開restoredatabase
    echo "リストア前のサイトURLは、${backup_newsiteurl}です。"

	echo データベースのリストアを行う
echo "wp db import ${DEST_BACKUP_DIR}${backup_fnamemysqldata2} ${ALLOW_ROOT}"
	wp db import ${DEST_BACKUP_DIR}${backup_fnamemysqldata2} ${ALLOW_ROOT}

    current_siteurl=`wp option get siteurl ${ALLOW_ROOT}`    #WP設定のサイトURLを取得する
    echo "バックアップデータのサイトURLは、${new_siteurl}です。"

	#最終リストアにファイル名を設定する（リストア直後に最終リストア情報を更新する）
	if [ -n "${ARG_TARGET_FILENAME}" ]; then
		echo "データベースの最終リストア情報を更新する（${ARG_TARGET_FILENAME}）"
		wp option update backup_last_restore_file ${ARG_TARGET_FILENAME} ${ALLOW_ROOT}

echo "データベースのリストアを完了しました"
	fi
#logファイルを作成する
echo `date '+%Y%m%d_%H%M%S'`: "データベースのリストアを完了しました。"  >> ${WP_CONTENT_DIR}${LOG_FILE}

#################################################
#URLを変更する
#################################################
#URL変更有無を判断する
echo "サイトURL判定(${current_siteurl} <=> ${backup_newsiteurl})"
if [ ${current_siteurl} != ${backup_newsiteurl} ];  then
	#以下に展開modifyurl
	echo "サイトURLを移行先のURL(${current_siteurl}⇒${backup_newsiteurl})に変更する"
	#wp option update siteurl ${backup_newsiteurl} ${ALLOW_ROOT}
	#wp option update home ${backup_newsiteurl} ${ALLOW_ROOT}
	wp search-replace ${current_siteurl} ${backup_newsiteurl} ${ALLOW_ROOT}

    echo "データベースのURLをすべてリストア先のURLに変更しました"
#logファイルを作成する
echo `date '+%Y%m%d_%H%M%S'`: "データベースのURL変換を完了しました。"  >> ${WP_CONTENT_DIR}${LOG_FILE}
fi

##################################################
#SSH関連情報を更新前のデータベース情報で上書きする
##################################################
wp option update backup_srchomepath ${backup_srchomepath} ${ALLOW_ROOT}	#転送元ドキュメントルートパス
wp option update backup_sshsrchostname ${backup_sshsrchostname} ${ALLOW_ROOT}	#ssh転送元ホスト名
wp option update backup_sshsrcportno ${backup_sshsrcportno} ${ALLOW_ROOT}	#ssh転送元ポートNo
wp option update backup_sshsrcusername ${backup_sshsrcusername} ${ALLOW_ROOT}	#ssh転送元ユーザ名
wp option update backup_sshsrcprivatekeypath ${backup_sshsrcprivatekeypath} ${ALLOW_ROOT}	#ssh転送元プライベートキーパス
wp option update backup_sshsrcprivatekeyname ${backup_sshsrcprivatekeyname} ${ALLOW_ROOT}	#ssh転送元プライベートキー名称
wp option update backup_sshnotification ${backup_sshnotification} ${ALLOW_ROOT}	#ssh経由バックアップ完了通知 1する,0しない
wp option update backup_production_dmain ${backup_production_dmain} ${ALLOW_ROOT}	#本番環境 ドメイン名
wp option update backup_debugmode ${backup_debugmode} ${ALLOW_ROOT}	#開発環境 ログ出力
wp option update backup_ajaxmode ${backup_ajaxmode} ${ALLOW_ROOT}	#開発環境 AJAX通信
wp option update backup_vagrantmode ${backup_vagrantmode} ${ALLOW_ROOT}	#開発環境 VAGRANTモード
wp option update backup_scd_execmode ${backup_scd_execmode} ${ALLOW_ROOT}	#スケジューラ 自動実行モード


#logファイルを作成する
echo `date '+%Y%m%d_%H%M%S'`": リストアを正常に終了しました" > ${WP_CONTENT_DIR}${LOG_FILE}
echo "おかえりなさい！<br />リストアをすべて完了しています！" > ${WP_CONTENT_DIR}${COMPLETE_FILE}

echo "リストア処理を完了しました"

#セッションが切れるためポップアップの完了画面は表示しない。
#echo ${SYSDATE} > ${WP_CONTENT_DIR}${COMPLETE_FILE}
