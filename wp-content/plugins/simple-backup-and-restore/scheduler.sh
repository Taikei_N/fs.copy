#!/bin/bash

# scheduler.sh
#
# -----------------------------------------------------------------------------
# Title : 予約実行処理
# -----------------------------------------------------------------------------
# 2018.10.13 created
#
# Description :
#   予約実行の設定をする
#
# Arguments :
#   [Arg1] WP_CONTENT_DIR
#
# Return value :
#   =0: OK
#   >0: ERROR CODE
#
#
# -----------------------------------------------------------------------------

#################################################
# 次回起動する日時を設定する
# IN:backup_scd_(date/day/hour)
# OUT:backup_scd_target
#################################################
function calcnextschedule(){
    backup_scd_month=`wp option get backup_scd_month ${ALLOW_ROOT}`    #month
    backup_scd_day=`wp option get backup_scd_day ${ALLOW_ROOT}`    #day
    backup_scd_date=`wp option get backup_scd_date ${ALLOW_ROOT}`    #date
    backup_scd_hour=`wp option get backup_scd_hour ${ALLOW_ROOT}`    #hour
    backup_scd_minute=`wp option get backup_scd_minute ${ALLOW_ROOT}`    #minute
    printf "秒:%s 時:%s 日:%s 月:%s 曜日:%s\n" "$backup_scd_minute" "$backup_scd_hour" "$backup_scd_date" "$backup_scd_month" "$backup_scd_day"
    #次に起動する日時を算出する

    if [ "${backup_scd_minute}" = "*" ]; then
        backup_scd_target="2099/12/31 23:59"
        echo "自動起動しない"
    elif [ "${backup_scd_hour}" = "*" ]; then
        backup_scd_target=`date -d '1 hours' +"%Y-%m-%d %H:${backup_scd_minute}"`
        echo "１時間間隔"
    elif [ "${backup_scd_date}" = "*" ] && [ "${backup_scd_day}" = "*" ]; then
        backup_scd_target=`date -d '1 days' +"%Y-%m-%d ${backup_scd_hour}:${backup_scd_minute}"`
        echo "１日間隔"
    elif [ "${backup_scd_date}" != "*" ] && [ "${backup_scd_day}" = "*" ]; then
        backup_scd_target=`date -d '1 months'  +"%Y-%m-${backup_scd_date} ${backup_scd_hour}:${backup_scd_minute}"`
        echo "１月間隔"
    elif [ "${backup_scd_date}" = "*" ] && [ "${backup_scd_day}" != "*" ]; then
            if [ ${backup_scd_day} -eq 0 ]; then
                backup_scd_target=`date -d 'next sunday'  +"%Y-%m-%d ${backup_scd_hour}:${backup_scd_minute}"`
            elif [ ${backup_scd_day} -eq 1 ]; then
                backup_scd_target=`date -d 'next monday'  +"%Y-%m-%d ${backup_scd_hour}:${backup_scd_minute}"`
            elif [ ${backup_scd_day} -eq 2 ]; then
                backup_scd_target=`date -d 'next tuesday'  +"%Y-%m-%d ${backup_scd_hour}:${backup_scd_minute}"`
            elif [ ${backup_scd_day} -eq 3 ]; then
                backup_scd_target=`date -d 'next wednesday'  +"%Y-%m-%d ${backup_scd_hour}:${backup_scd_minute}"`
            elif [ ${backup_scd_day} -eq 4 ]; then
                backup_scd_target=`date -d 'next thursday'  +"%Y-%m-%d ${backup_scd_hour}:${backup_scd_minute}"`
            elif [ ${backup_scd_day} -eq 5 ]; then
                backup_scd_target=`date -d 'next friday'  +"%Y-%m-%d ${backup_scd_hour}:${backup_scd_minute}"`
            else
                backup_scd_target=`date -d 'next saturday'  +"%Y-%m-%d ${backup_scd_hour}:${backup_scd_minute}"`
            fi
            echo "１週間間隔"
    else
        backup_scd_target="2099/12/31 23:59"
        echo "自動起動しない"
    fi
}

#################################################
# スケジュールスクリプト
#
#
#################################################

# 実行時に指定された引数の数、つまり変数 $# の値が 3 でなければエラー終了。

if [ $# -eq 1 ]; then
    ARG_CURRENT_PATH=$1
    arg1=0
    arg2=0
	arg3=""
    #echo 引数が1個設定されました
elif [ $# -eq 2 ]; then
    ARG_CURRENT_PATH=$1
    arg1=$2
    arg2=0
	arg3=""
    #echo 引数が2個設定されました
elif [ $# -eq 3 ]; then
    ARG_CURRENT_PATH=$1
    arg1=$2
    arg2=$3
	arg3=""
    #echo 引数が3個設定されました
elif [ $# -eq 4 ]; then
    ARG_CURRENT_PATH=$1
    arg1=$2
    arg2=$3
    arg3=$4
    #echo 引数が4個設定されました
else
    echo "引数の数が不正です"
    exit 1
fi


####################################################
# 初期設定
####################################################
#カレントディレクトリをWP-CONTENTに移動する
cd ${ARG_CURRENT_PATH}

#WP-CONTENTパスを取得する
WP_CONTENT_DIR=$1/

#タイムスタンプを取得、変数に設定する
#現在日時
curdate=`date +"%Y-%m-%d %H:%M"`
SYSDATE=`date '+%Y%m%d_%H%M%S_'`

#ROOT権限デフォルト値設定
ALLOW_ROOT=""
#WP設定画面で設定したスクリプトを読み込む
source ${WP_CONTENT_DIR}backup_addtional_script.sh

#エラー判定のため次回実行日時の読み込みを行う
backup_scd_target_org=`wp option get backup_scd_target ${ALLOW_ROOT}`    #next exec date

if [ ${#backup_scd_target_org} -eq 0 ]; then
#logファイルを作成する
echo "$curdate スケジューラの自動スクリプトを中断しました"
exit 1
fi

if [ $arg1 -eq 1 ]; then
    #次に実行する日時を計算する
    #backup_scd_targetに予約日時が設定される
    calcnextschedule
    wp option update backup_scd_target "$backup_scd_target" ${ALLOW_ROOT}
    printf "次回起動日時 %s\n" "`wp option get backup_scd_target ${ALLOW_ROOT}`"
    exit 0
else
    backup_scd_target=`wp option get backup_scd_target ${ALLOW_ROOT}`    #next exec date
    backup_scd_execmode=`wp option get backup_scd_execmode ${ALLOW_ROOT}`    #exec mode
    backup_myplugin_dir=`wp option get backup_myplugin_dir ${ALLOW_ROOT}`    #my plugin directory
fi

#現在日時と次回起動日時を出力する
printf "現在日時 %s  次回起動日時 %s\n" "$curdate" "$backup_scd_target"

    curdate_unxtime=`date +%s --date "$curdate"` 
    target_unxtime=`date +%s --date "$backup_scd_target"`
    if [ $curdate_unxtime -ge $target_unxtime ]; then
        if [ "$backup_scd_execmode" = "1" ]; then
            echo "サーバデータ受信を起動する: bash ${ARG_CURRENT_PATH}/ssh_transfer.sh ${ARG_CURRENT_PATH}"
            bash ${ARG_CURRENT_PATH}/ssh_transfer.sh ${ARG_CURRENT_PATH} 1
        elif [ "$backup_scd_execmode" = "2" ]; then
            echo "サーバデータ受信を起動する: bash ${ARG_CURRENT_PATH}/ssh_transfer.sh ${ARG_CURRENT_PATH}"
            bash ${ARG_CURRENT_PATH}/ssh_transfer.sh ${ARG_CURRENT_PATH} 1
            echo "リストアを起動する: ${ARG_CURRENT_PATH}/restore.sh ${ARG_CURRENT_PATH} ${backup_myplugin_dir}"
            bash ${ARG_CURRENT_PATH}/restore.sh ${ARG_CURRENT_PATH} ${backup_myplugin_dir}
        else
            echo "バックアップを起動する: bash ${ARG_CURRENT_PATH}/backup.sh ${ARG_CURRENT_PATH} 1"
            bash ${ARG_CURRENT_PATH}/backup.sh ${ARG_CURRENT_PATH} 1
        fi
        #次に実行する日時を計算する
        calcnextschedule
        wp option update backup_scd_target "$backup_scd_target" ${ALLOW_ROOT}
        printf "次回起動日時 %s\n" "`wp option get backup_scd_target ${ALLOW_ROOT}`"

    else
        printf "%s 起動しない\n" "`date +"%Y-%m-%d %H:%M"`"
        exit 0
    fi


exit 0


#date +%s --date "2018/10/10 1:1"
#date +%s --date "2017/10/10 1:1"
#date +%s --date "2018/11/01 1:1"
#date +%s --date "1:1"



