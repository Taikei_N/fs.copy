<?php
/*
Plugin Name: Easy Backup and Restore（簡単！バックアップ＆リストア）
Plugin URI: http://www.sinceretechnology.com.au/simple-backup-restore-ja/
Description: 簡単！バックアップ＆リストアプラグイン
Author: H.Matsubara
Version: 1.01
Author URI: http://www.sinceretechnology.com.au/
Notes:
1.01 js(jQuery) refined
*/

  //定数の宣言
  require_once (plugin_dir_path( __FILE__ ).'const.php');
  //共通クラスの宣言（GLOBAL.PHPにも同様にGLOBAL宣言して使用するクラスで読み込む）
  //なし
  //投稿画面で使用するJSを対象のショートコードが使われている場合のみ読み込みとインスタンス化をする
  require_once(plugin_dir_path( __FILE__ )."includes/CommonModule_LoadScripts.php");
  $GlobalCommonModule_LoadScripts = new CommonModule_LoadScripts(CONST_SIMPLE_BACKUP_RESTORE_DOMAIN);
  //プラグイン用クラスの読み込みとインスタンス化する
  require_once(plugin_dir_path( __FILE__ )."myPluginDatabaseClass.php");
  $GlobalmyPluginDatabase = new myPluginDatabaseClass();
  //プラグイン用クラスの読み込みとインスタンス化する
  require_once(plugin_dir_path( __FILE__ )."myPluginMainClass.php");
  $GlobalmyPluginMain = new myPluginMainClass();



  //当プラグインのメイン処理をインスタンス化し実行する
  $mySimpleBackupRestorePlugin = new mySimpleBackupRestorePluginClass();

/************************************************************
* Class mySimpleBackupRestorePluginClass
************************************************************/
class mySimpleBackupRestorePluginClass
{
  /************************************************************
   * mySimpleBackupRestorePluginClass constructor.
   *
   ************************************************************/
  function __construct()
  {
    //スケジューラで使用するため自身のプラグインフォルダ名をオプションに作成する
    update_option("backup_myplugin_dir", CONST_SIMPLE_BACKUP_RESTORE_DOMAIN);

    //グローバル変数宣言（※補完されない）
    //require(plugin_dir_path( __FILE__ ).'global.php');
    //TODO: SHELL SCRIPT実行時にWP-CLIが呼ばれるときになぜか次の行でエラーが発生する。シェル実行時にPLUGINクラスが実行される原因を調べること
    global $GlobalCommonModule_LoadScripts;
    if ($GlobalCommonModule_LoadScripts) {
      $GlobalCommonModule_LoadScripts->LoadScripts(array($this, 'load_scripts_amdin'), false, false, false);
    }

    //プラグインが有効化されたときに起動する関数を登録する
    register_activation_hook(__FILE__, array($this, 'activate_myPlugin'));

    //プラグインが無効化されたときに起動する関数を登録する
    register_deactivation_hook(__FILE__, array($this, 'deactivate_myPlugin'));

    //管理メニューに登録する
    add_action('admin_menu', array($this, 'add_pages'));

    //翻訳ファイルを読み込む
    add_action('plugins_loaded', array($this, 'load_language_files'));

    //管理画面用AJAXコールバック関数を登録する
    //プラグイン用クラスの読み込みとインスタンス化する
    require_once(plugin_dir_path( __FILE__ )."myPluginAjaxClass.php");
    $GlobalmyPluginAjax = new myPluginAjaxClass();
    add_action('wp_ajax_' . CONST_SIMPLE_BACKUP_RESTORE_DOMAIN . '-admin', array($GlobalmyPluginAjax, 'my_ajax'));

    //プラグイン一覧の対象プラグインに設定のリンクを表示する
    add_filter('plugin_action_links_' . plugin_basename(__FILE__), array($this, 'backup_settings_link'));

  }

  /************************************************************
   *　管理画面用スクリプト読み込み処理
   ************************************************************/
  function load_scripts_amdin()
  {
    //  JQUERY UIはすでにバンドル済み
    wp_enqueue_script('jquery-ui-dialog');
    wp_enqueue_script('jquery-ui-tooltip');
    // 'myajax-js'の読み込み時にBASEのハンドルにjquery-ui-dialogを指定しているため、同梱のJQUERYｰUiダイアログが呼び出される
    wp_enqueue_script(CONST_SIMPLE_BACKUP_RESTORE_DOMAIN . '-admin-js', plugins_url(CONST_SIMPLE_BACKUP_RESTORE_DOMAIN) . '/js/' . CONST_SIMPLE_BACKUP_RESTORE_DOMAIN . '-admin.js', array('jquery-ui-dialog', 'jquery-ui-tooltip'), '1.0.0', true);

    //スタイルシートの読み込み
    //【重要】最後の引数は必ずFALSEにすること！！！！
    wp_enqueue_style('jquery-ui-dialog-min-css', includes_url() . 'css/jquery-ui-dialog.min.css');
    wp_enqueue_style(CONST_SIMPLE_BACKUP_RESTORE_DOMAIN . '-admin-css', plugins_url(CONST_SIMPLE_BACKUP_RESTORE_DOMAIN) . '/css/' . CONST_SIMPLE_BACKUP_RESTORE_DOMAIN . '-admin.css', array('jquery-ui-dialog-min-css'), '1.0.0', false);

    /************************************************************
     * Ajaxを利用するためリクエスト送信先URLをグローバル変数として定義
     * 下記サイトを参照
     * http://www.nakamurayuji.com/archives/21421
     * https://chaika.hatenablog.com/entry/2017/09/08/090000
     ************************************************************/
    $ajaxMode = @get_option("backup_ajaxmode");
    $debugMode = @get_option("backup_debugmode") == "1" ? true : false;
    // 配列をJSのオブジェクトに変換したscriptを出力する
    // 【重要】wp_localize_script第一引数には、既にwp_enqueue_style関数のスクリプト名を指定する。
    //$acrion = 'my-ajax-action';
    //$acrion2 = 'my-ajax-action-reply';
    $action = CONST_SIMPLE_BACKUP_RESTORE_DOMAIN . '-admin';
    //$my_id = SwpmMemberUtils::get_logged_in_members_id();
    wp_localize_script(CONST_SIMPLE_BACKUP_RESTORE_DOMAIN . '-admin-js', 'SimpleBackupRestoreScriptData', [
      'api' => admin_url('admin-ajax.php'),
      //'action_reply' => $acrion2,
      //'nonce_reply'  => wp_create_nonce( $acrion2 ),
      'action' => $action,
      'nonce' => wp_create_nonce($action),
      'ajaxMode' => $ajaxMode,
      'debugMode' => $debugMode,
      //'my_id'  => $my_id,
    ]);
  }

  /************************************************************
   *　プラグイン一覧の停止の横に設定を追加する
   ************************************************************/
  function backup_settings_link($links)
  {
    $settings_link = '<a href="' . esc_url(add_query_arg(array('page' => CONST_SIMPLE_BACKUP_RESTORE_ID, 'tabclicked' => 'setting'), admin_url('admin.php'))) . '">' . __('Setting', CONST_SIMPLE_BACKUP_RESTORE_DOMAIN) . '</a>';
    array_unshift($links, $settings_link);
    return $links;
  }

  function load_language_files()
  {
    //参考 https://nskw-style.com/2014/wordpress/code-stealing/loading-translation-files.html
    //load_plugin_textdomain( CONST_SIMPLE_BACKUP_RESTORE_DOMAIN, false, dirname( plugin_basename( __FILE__ ) ) . '/languages/');
    //参考 https://wpdocs.osdn.jp/I18n_for_WordPress_Developers#.E3.83.86.E3.82.AD.E3.82.B9.E3.83.88.E3.83.89.E3.83.A1.E3.82.A4.E3.83.B3
    load_plugin_textdomain(CONST_SIMPLE_BACKUP_RESTORE_DOMAIN, false, basename(dirname(__FILE__)) . '/languages/');
  }


  /************************************************************
   * 管理画面にメニュー登録処理
   ************************************************************/
  function add_pages()
  {
    global $GlobalmyPluginMain;
    //管理画面サイドバー直下にメニューを追加する
    add_menu_page(__('EASY! Backup & Restore', CONST_SIMPLE_BACKUP_RESTORE_DOMAIN), __('EASY! Backup & Restore', CONST_SIMPLE_BACKUP_RESTORE_DOMAIN), 'level_8', CONST_SIMPLE_BACKUP_RESTORE_ID, array($GlobalmyPluginMain, 'main'), 'dashicons-thumbs-up', 25);
    //管理画面サイドバー直下にサブメニューを追加する
    add_submenu_page(CONST_SIMPLE_BACKUP_RESTORE_ID, __('Backup', CONST_SIMPLE_BACKUP_RESTORE_DOMAIN),  __('Backup', CONST_SIMPLE_BACKUP_RESTORE_DOMAIN), 'manage_options', CONST_SIMPLE_BACKUP_RESTORE_ID . '&tabclicked=backup', array($GlobalmyPluginMain, 'main'));
    add_submenu_page(CONST_SIMPLE_BACKUP_RESTORE_ID, __('Restore', CONST_SIMPLE_BACKUP_RESTORE_DOMAIN), __('Restore', CONST_SIMPLE_BACKUP_RESTORE_DOMAIN), 'manage_options', CONST_SIMPLE_BACKUP_RESTORE_ID . '&tabclicked=restore', array($GlobalmyPluginMain, 'main'));
    add_submenu_page(CONST_SIMPLE_BACKUP_RESTORE_ID, __('Files', CONST_SIMPLE_BACKUP_RESTORE_DOMAIN), __('Files', CONST_SIMPLE_BACKUP_RESTORE_DOMAIN), 'manage_options', CONST_SIMPLE_BACKUP_RESTORE_ID . '&tabclicked=transfer', array($GlobalmyPluginMain, 'main'));
    add_submenu_page(CONST_SIMPLE_BACKUP_RESTORE_ID, __('scheduler', CONST_SIMPLE_BACKUP_RESTORE_DOMAIN), __('scheduler', CONST_SIMPLE_BACKUP_RESTORE_DOMAIN), 'manage_options', CONST_SIMPLE_BACKUP_RESTORE_ID . '&tabclicked=scheduler', array($GlobalmyPluginMain, 'main'));
    add_submenu_page(CONST_SIMPLE_BACKUP_RESTORE_ID, __('SSH setting', CONST_SIMPLE_BACKUP_RESTORE_DOMAIN), __('SSH setting', CONST_SIMPLE_BACKUP_RESTORE_DOMAIN), 'manage_options', CONST_SIMPLE_BACKUP_RESTORE_ID . '&tabclicked=ssh_setting', array($GlobalmyPluginMain, 'main'));
    add_submenu_page(CONST_SIMPLE_BACKUP_RESTORE_ID, __('General setting', CONST_SIMPLE_BACKUP_RESTORE_DOMAIN), __('General setting', CONST_SIMPLE_BACKUP_RESTORE_DOMAIN), 'manage_options', CONST_SIMPLE_BACKUP_RESTORE_ID . '&tabclicked=setting', array($GlobalmyPluginMain, 'main'));
    add_submenu_page(CONST_SIMPLE_BACKUP_RESTORE_ID, __('information', CONST_SIMPLE_BACKUP_RESTORE_DOMAIN), __('information', CONST_SIMPLE_BACKUP_RESTORE_DOMAIN), 'manage_options', CONST_SIMPLE_BACKUP_RESTORE_ID . '&tabclicked=information', array($GlobalmyPluginMain, 'main'));
  }


  /************************************************************
   * バックアップリストアプラグインのプラグインインストール時実行関数
   *
   * プラグインインストール時に実行する処理を記述する
   ************************************************************/
  function activate_myPlugin()
  {
    //プラグイン用クラスの読み込みとインスタンス化する
    require_once(plugin_dir_path( __FILE__ )."myPluginActivationClass.php");
    $myPluginActivation = new myPluginActivationClass();
    $myPluginActivation->activation();
  }

  /************************************************************
   * バックアップリストアプラグインのプラグインインストール時実行関数
   *
   * プラグインアンインストール時に実行する処理を記述する
   ************************************************************/
  function deactivate_myPlugin()
  {
    //プラグイン用クラスの読み込みとインスタンス化する
    require_once(plugin_dir_path( __FILE__ )."myPluginActivationClass.php");
    $myPluginActivation = new myPluginActivationClass();
    $myPluginActivation->deactivation();
  }

}
