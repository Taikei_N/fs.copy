#################################################
#SSH転送によりバックアップファイルをローカルに受信する
#################################################
function recievebackupdata(){

	#バックアップフォルダの前回転送ファイルを削除する
	rm -f  ${TEMP_BACKUP_DIR}/${INFODATA_FILE}

	#本番サーバーからupdateファイルを転送する
	echo "scp -o 'StrictHostKeyChecking=no' -i ${backup_sshsrcprivatekeypath}${backup_sshsrcprivatekeyname} -P ${backup_sshsrcportno} ${backup_sshsrcusername}@${backup_sshsrchostname}:${SRC_BACKUP_DIR}/${INFODATA_FILE} ${TEMP_BACKUP_DIR}"
	scp  -o "StrictHostKeyChecking=no" -i ${backup_sshsrcprivatekeypath}${backup_sshsrcprivatekeyname} -P ${backup_sshsrcportno} ${backup_sshsrcusername}@${backup_sshsrchostname}:${SRC_BACKUP_DIR}/${INFODATA_FILE} ${TEMP_BACKUP_DIR}

	#起動中のスクリプトを確認する
	if [ ! -e ${TEMP_BACKUP_DIR}/${INFODATA_FILE} ]; then
	  #logファイルを作成する
	  echo `date '+%Y%m%d_%H%M%S'`: aborted >> ${TEMP_BACKUP_DIR}${LOG_FILE}
      echo "バックアップファイルが見つかりませんでした"
	exit 1
	fi
    echo "ステータスファイルを受信しました"

	#バックアップフォルダの前回転送ファイルを削除する
	rm -f ${TEMP_BACKUP_DIR}/${backup_fnamemysqldata2}
	rm -f ${TEMP_BACKUP_DIR}/${backup_fnamewpthemes2}
	rm -f ${TEMP_BACKUP_DIR}/${backup_fnamewpplugins2}
	rm -f ${TEMP_BACKUP_DIR}/${backup_fnamewpuploads2}


	#本番サーバーからDBファイルを転送する
 	echo "scp -o 'StrictHostKeyChecking=no' -i ${backup_sshsrcprivatekeypath}${backup_sshsrcprivatekeyname} -P ${backup_sshsrcportno} ${backup_sshsrcusername}@${backup_sshsrchostname}:${SRC_BACKUP_DIR}/${backup_fnamemysqldata2} ${TEMP_BACKUP_DIR}"
 	scp  -o "StrictHostKeyChecking=no" -i ${backup_sshsrcprivatekeypath}${backup_sshsrcprivatekeyname} -P ${backup_sshsrcportno} ${backup_sshsrcusername}@${backup_sshsrchostname}:${SRC_BACKUP_DIR}/${backup_fnamemysqldata2} ${TEMP_BACKUP_DIR}
	if [ $? -eq 0 ]; then
		echo "WordPressデータベースバックアップファイルを受信しました"
	else
		echo "WordPressデータベースバックアップファイルの受信に失敗しました"
		exit 1
	fi
    

	#本番サーバーからwp-themesファイルを転送する
 	echo "scp -o 'StrictHostKeyChecking=no' -i ${backup_sshsrcprivatekeypath}${backup_sshsrcprivatekeyname} -P ${backup_sshsrcportno} ${backup_sshsrcusername}@${backup_sshsrchostname}:${SRC_BACKUP_DIR}/${backup_fnamewpthemes2} ${TEMP_BACKUP_DIR}"
	scp  -o "StrictHostKeyChecking=no" -i ${backup_sshsrcprivatekeypath}${backup_sshsrcprivatekeyname} -P ${backup_sshsrcportno} ${backup_sshsrcusername}@${backup_sshsrchostname}:${SRC_BACKUP_DIR}/${backup_fnamewpthemes2} ${TEMP_BACKUP_DIR}
	if [ $? -eq 0 ]; then
    	echo "WordPressテーマバックアップファイルを受信しました"
	else
		echo "WordPressデータベースバックアップファイルの受信に失敗しました"
		exit 1
	fi

	#本番サーバーからwp-pluginsファイルを転送する
    echo "scp -o 'StrictHostKeyChecking=no' -i ${backup_sshsrcprivatekeypath}${backup_sshsrcprivatekeyname} -P ${backup_sshsrcportno} ${backup_sshsrcusername}@${backup_sshsrchostname}:${SRC_BACKUP_DIR}/${backup_fnamewpplugins2} ${TEMP_BACKUP_DIR}"
	scp  -o "StrictHostKeyChecking=no" -i ${backup_sshsrcprivatekeypath}${backup_sshsrcprivatekeyname} -P ${backup_sshsrcportno} ${backup_sshsrcusername}@${backup_sshsrchostname}:${SRC_BACKUP_DIR}/${backup_fnamewpplugins2} ${TEMP_BACKUP_DIR}
	if [ $? -eq 0 ]; then
    	echo "WordPressプラグインファイルを受信しました"
	else
		echo "WordPressデータベースバックアップファイルの受信に失敗しました"
		exit 1
	fi

	#本番サーバーからwp-uploadsファイルを転送する
	echo "scp -o 'StrictHostKeyChecking=no' -i ${backup_sshsrcprivatekeypath}${backup_sshsrcprivatekeyname} -P ${backup_sshsrcportno} ${backup_sshsrcusername}@${backup_sshsrchostname}:${SRC_BACKUP_DIR}/${backup_fnamewpuploads2} ${TEMP_BACKUP_DIR}"
	scp  -o "StrictHostKeyChecking=no" -i ${backup_sshsrcprivatekeypath}${backup_sshsrcprivatekeyname} -P ${backup_sshsrcportno} ${backup_sshsrcusername}@${backup_sshsrchostname}:${SRC_BACKUP_DIR}/${backup_fnamewpuploads2} ${TEMP_BACKUP_DIR}
	if [ $? -eq 0 ]; then
    	echo "WordPressアップロードメディアバックアップファイルを受信しました"
	else
		echo "WordPressデータベースバックアップファイルの受信に失敗しました"
		exit 1
	fi

	#本番サーバーからwp-configファイルを転送する
	echo "scp -o 'StrictHostKeyChecking=no' -i ${backup_sshsrcprivatekeypath}${backup_sshsrcprivatekeyname} -P ${backup_sshsrcportno} ${backup_sshsrcusername}@${backup_sshsrchostname}:${SRC_BACKUP_DIR}/${backup_fnamewpconfig2} ${TEMP_BACKUP_DIR}"
	scp  -o "StrictHostKeyChecking=no" -i ${backup_sshsrcprivatekeypath}${backup_sshsrcprivatekeyname} -P ${backup_sshsrcportno} ${backup_sshsrcusername}@${backup_sshsrchostname}:${SRC_BACKUP_DIR}/${backup_fnamewpconfig2} ${TEMP_BACKUP_DIR}
	if [ $? -eq 0 ]; then
    	echo "WordPress wp-configファイルを受信しました"
	else
		echo "WordPress wp-configファイルの受信に失敗しました"
		exit 1
	fi

}




#################################################
# SSH転送シェルスクリプト
#
#
#################################################


if [ $# -eq 1 ]; then
    ARG_MY_PLUGIN_NAME=$1
    ARG_AUTOEXEC_MODE=0
    echo 引数が1個設定されました
 elif [ $# -eq 2 ]; then
     ARG_MY_PLUGIN_NAME=$1
     ARG_AUTOEXEC_MODE=$2
     echo 引数が2個設定されました
# elif [ $# -eq 3 ]; then
#     ARG_MY_PLUGIN_NAME=$1
#     arg1=$2
#     arg2=$3
# 	arg3=""
#     echo 引数が3個設定されました
# elif [ $# -eq 4 ]; then
#     ARG_MY_PLUGIN_NAME=$1
#     arg1=$2
#     arg2=$3
#     arg3=$4
#     echo 引数が4個設定されました
else
    echo "リストアする場合は、必ず２つ以下の引数を設定してください"
    echo "※第一引数には必ず自身のプラグイン名を設定してください"
fi


####################################################
# 変数の設定
####################################################
#カレントディレクトリをWP-CONTENTに移動する
cd ${ARG_MY_PLUGIN_NAME}

#パスを通す
export PATH=$PATH:/usr/local/bin/

#ROOT権限の場合はこのオプションをつけないとエラーになる
ALLOW_ROOT="--allow-root"

#logファイルを作成する
LOG_FILE="backup-process-status.txt"
COMPLETE_FILE="complete.txt"
WP_CONTENT_DIR=`wp eval "echo WP_CONTENT_DIR;" ${ALLOW_ROOT}`
CURRENT_BACKUP_DIR="${WP_CONTENT_DIR}/"

rm -f ${CURRENT_BACKUP_DIR}${LOG_FILE}
touch ${CURRENT_BACKUP_DIR}${LOG_FILE}
chown apache:apache ${CURRENT_BACKUP_DIR}${LOG_FILE}
chmod 777 ${CURRENT_BACKUP_DIR}${LOG_FILE}

echo `date '+%Y%m%d_%H%M%S'`: サーバーデータ受信処理を開始しました >> ${CURRENT_BACKUP_DIR}${LOG_FILE}
    echo サーバーデータ受信処理を開始しました


#タイムスタンプを取得、変数に設定する
SYSDATE=`date '+%Y%m%d_%H%M%S_'`


backup_srchomepath=`wp option get backup_srchomepath ${ALLOW_ROOT}`    #転送元（バックアップ）データパス
echo $backup_srchomepath
backup_sshsrchostname=`wp option get backup_sshsrchostname ${ALLOW_ROOT}`    #ssh転送元ホスト名
echo $backup_sshsrchostname
backup_sshsrcportno=`wp option get backup_sshsrcportno ${ALLOW_ROOT}`    #ssh転送元ポートNo
echo $backup_sshsrcportno
backup_sshsrcusername=`wp option get backup_sshsrcusername ${ALLOW_ROOT}`    #ssh転送元ユーザ名
echo $backup_sshsrcusername
backup_sshsrcprivatekeypath=`wp option get backup_sshsrcprivatekeypath ${ALLOW_ROOT}`    #ssh転送元プライベートキーパス
echo $backup_sshsrcprivatekeypath
backup_sshsrcprivatekeyname=`wp option get backup_sshsrcprivatekeyname ${ALLOW_ROOT}`    #ssh転送元プライベートキー名称
echo $backup_sshsrcprivatekeyname
backup_sshnotification=`wp option get backup_sshnotification ${ALLOW_ROOT}`    #ssh経由バックアップ完了通知 1する,0しない
echo $backup_sshnotification




backup_srcdirname=`wp option get backup_srcdirname ${ALLOW_ROOT}`    #転送元（バックアップ）ディレクトリ名
echo $backup_srcdirname
backup_destdirname=`wp option get backup_destdirname ${ALLOW_ROOT}`    #転送先（復元）ディレクトリ名
echo $backup_destdirname
backup_tempdirname=`wp option get backup_tempdirname ${ALLOW_ROOT}`    #一時ディレクトリ名
echo $backup_tempdirname
backup_recieveddirname=`wp option get backup_recieveddirname ${ALLOW_ROOT}`    #一時ディレクトリ名
echo $backup_recieveddirname

backup_fnamemysqldata=`wp option get backup_fnamemysqldata ${ALLOW_ROOT}`    #データベース圧縮ファイル名
echo $backup_fnamemysqldata
backup_fnamewpthemes=`wp option get backup_fnamewpthemes ${ALLOW_ROOT}`    #WPテーマ圧縮ファイル名
echo $backup_fnamewpthemes
backup_fnamewpplugins=`wp option get backup_fnamewpplugins ${ALLOW_ROOT}`    #WPプラグイン圧縮ファイル名
echo $backup_fnamewpplugins
backup_fnamewpuploads=`wp option get backup_fnamewpuploads ${ALLOW_ROOT}`    #WPアップロード圧縮ファイル名
echo $backup_fnamewpuploads
backup_fnameinfodata=`wp option get backup_fnameinfodata ${ALLOW_ROOT}`    #バックアップ情報ファイル名
echo $backup_fnameinfodata
backup_fnamewpconfig=`wp option get backup_fnamewpconfig ${ALLOW_ROOT}`    #wp-configファイル名
echo $backup_fnamewpconfig


#バックアップデータベースファイル名（日付あり）
backup_fnamemysqldata1=${backup_fnamemysqldata//\$\{SYSDATE\}/${SYSDATE}}
#バックアップＷＰテーマファイル名（日付あり）
backup_fnamewpthemes1=${backup_fnamewpthemes//\$\{SYSDATE\}/${SYSDATE}}
#バックアップＷＰプラグインファイル名（日付あり）
backup_fnamewpplugins1=${backup_fnamewpplugins//\$\{SYSDATE\}/${SYSDATE}}
#バックアップＷＰアップロードファイル名（日付あり）
backup_fnamewpuploads1=${backup_fnamewpuploads//\$\{SYSDATE\}/${SYSDATE}}
#バックアップ情報ファイル名（日付あり）
backup_fnameinfodata1=${backup_fnameinfodata//\$\{SYSDATE\}/${SYSDATE}}
#wp-configファイル名（日付あり）
backup_fnamewpconfig1=${backup_fnamewpconfig//\$\{SYSDATE\}/${SYSDATE}}

#バックアップデータベースファイル名（日付なし）
backup_fnamemysqldata2=${backup_fnamemysqldata//\$\{SYSDATE\}/}
#バックアップＷＰテーマファイル名（日付なし）
backup_fnamewpthemes2=${backup_fnamewpthemes//\$\{SYSDATE\}/}
#バックアップＷＰプラグインファイル名（日付なし）
backup_fnamewpplugins2=${backup_fnamewpplugins//\$\{SYSDATE\}/}
#バックアップＷＰアップロードファイル名（日付なし）
backup_fnamewpuploads2=${backup_fnamewpuploads//\$\{SYSDATE\}/}
#バックアップ情報ファイル名（日付なし）
backup_fnameinfodata2=${backup_fnameinfodata//\$\{SYSDATE\}/}
#wp-configファイル名（日付なし）
backup_fnamewpconfig2=${backup_fnamewpconfig//\$\{SYSDATE\}/}

INFODATA_FILE=${backup_fnameinfodata2}


#ABSPATHのディレクトリパスを取得する
ABSPATH=`wp eval "echo ABSPATH;" ${ALLOW_ROOT}`

#SSH転送元サーバーバックアップディレクトリ
#DBパラメータは使用しない（ABSPATH使用）
#SRC_BACKUP_DIR=${backup_srchomepath}${backup_srcdirname}/
SRC_BACKUP_DIR=${backup_srchomepath}/${backup_srcdirname}
#一時ディレクトリ
#DBパラメータは使用しない（ABSPATH使用）
#TEMP_BACKUP_DIR=${backup_desthomepath}${backup_tempdirname}/
TEMP_BACKUP_DIR=${ABSPATH}${backup_tempdirname}
#受信データ用ディレクトリ
#DBパラメータは使用しない（ABSPATH使用）
#TEMP_BACKUP_DIR=${backup_desthomepath}${backup_tempdirname}/
RECIEVED_BACKUP_DIR=${ABSPATH}${backup_recieveddirname}


echo "オプションデータの読み込みを完了しました"
echo `date '+%Y%m%d_%H%M%S'`: "オプションデータの読み込みを完了しました"  >> ${CURRENT_BACKUP_DIR}${LOG_FILE}


#SSH転送
recievebackupdata
echo "SSH転送処理を完了しました"
echo `date '+%Y%m%d_%H%M%S'`: "SSH転送処理を完了しました"  >> ${CURRENT_BACKUP_DIR}${LOG_FILE}


if [ ! -e "${TEMP_BACKUP_DIR}/${backup_fnamemysqldata2}" ] \
	|| [ ! -e "${TEMP_BACKUP_DIR}/${backup_fnamewpthemes2}" ] \
	|| [ ! -e "${TEMP_BACKUP_DIR}/${backup_fnamewpplugins2}" ] \
	|| [ ! -e "${TEMP_BACKUP_DIR}/${backup_fnamewpuploads2}" ]; then
	echo "リストアに必要なバックアップファイルが不足しています"
	echo "処理を中断します"
	#一時フォルダの転送済みファイルを削除する
	rm -f ${TEMP_BACKUP_DIR}/${backup_fnamemysqldata2}
	rm -f ${TEMP_BACKUP_DIR}/${backup_fnamewpthemes2}
	rm -f ${TEMP_BACKUP_DIR}/${backup_fnamewpplugins2}
	rm -f ${TEMP_BACKUP_DIR}/${backup_fnamewpuploads2}
	rm -f ${TEMP_BACKUP_DIR}/${backup_fnamewpconfig2}
	exit 1
fi
#
#if [ $# -eq 4 ]; then
#    bash ${WP_CONTENT_DIR}restore.sh ${ARG_MY_PLUGIN_NAME} ${arg1} ${arg2} ${arg3}
#    echo "リストア処理を完了しました"
#else
#	echo "引数不足のため、リストア処理を中止しました"
#	exit 1
#fi

	#一時フォルダの受信ファイルを受信データ用フォルダに移動する
	mv -f ${TEMP_BACKUP_DIR}/${backup_fnamemysqldata2} ${RECIEVED_BACKUP_DIR}/${backup_fnamemysqldata1}
	mv -f ${TEMP_BACKUP_DIR}/${backup_fnamewpthemes2} ${RECIEVED_BACKUP_DIR}/${backup_fnamewpthemes1}
	mv -f ${TEMP_BACKUP_DIR}/${backup_fnamewpplugins2} ${RECIEVED_BACKUP_DIR}/${backup_fnamewpplugins1}
	mv -f ${TEMP_BACKUP_DIR}/${backup_fnamewpuploads2} ${RECIEVED_BACKUP_DIR}/${backup_fnamewpuploads1}
	mv -f ${TEMP_BACKUP_DIR}/${backup_fnameinfodata2} ${RECIEVED_BACKUP_DIR}/${backup_fnameinfodata1}
	mv -f ${TEMP_BACKUP_DIR}/${backup_fnamewpconfig2} ${RECIEVED_BACKUP_DIR}/${backup_fnamewpconfig1}

echo "受信データを受信データ用フォルダに移動しました"
echo `date '+%Y%m%d_%H%M%S'`: "受信データを受信データ用フォルダに移動しました"  >> ${CURRENT_BACKUP_DIR}${LOG_FILE}

echo "サーバーデータ受信処理をすべて完了しました"
echo `date '+%Y%m%d_%H%M%S'`: "サーバーデータ受信処理をすべて完了しました"  >> ${CURRENT_BACKUP_DIR}${LOG_FILE}

#完了ファイルを生成する
echo ${SYSDATE}: Completed
echo "サーバーデータ受信を正常に終了しました" > ${CURRENT_BACKUP_DIR}${LOG_FILE}

if [ "$ARG_AUTOEXEC_MODE" != "1" ]; then
    echo "サーバーデータの受信をすべて完了しました！" > ${CURRENT_BACKUP_DIR}${COMPLETE_FILE}
fi
exit 0
