#!/bin/bash

# script-test.sh
#
# -----------------------------------------------------------------------------
# Title : スクリプトが実行可能かどうかテストする処理
# -----------------------------------------------------------------------------
# 2018.10.13 created
#
# Description :
#   スクリプトが実行可能かどうかテストする
#
# Arguments :
#   [Arg1] WP_CONTENT_DIR
#
# Return value :
#   =0: OK
#   >0: ERROR CODE
#
#
# -----------------------------------------------------------------------------

#################################################
# テスト用シェルスクリプト
#
#
#################################################
NO_OF_ARGS=$#

if [ $# -eq 1 ]; then
    ARG_MY_PLUGIN_NAME=$1
    ARG_AUTOEXEC_MODE=0
    echo 引数が1個設定されました
 elif [ $# -eq 2 ]; then
     ARG_MY_PLUGIN_NAME=$1
     ARG_AUTOEXEC_MODE=$2
     echo 引数が2個設定されました
# elif [ $# -eq 3 ]; then
#     ARG_MY_PLUGIN_NAME=$1
#     arg1=$2
#     arg2=$3
# 	arg3=""
#     echo 引数が3個設定されました
# elif [ $# -eq 4 ]; then
#     ARG_MY_PLUGIN_NAME=$1
#     arg1=$2
#     arg2=$3
#     arg3=$4
#     echo 引数が4個設定されました
else
    echo "リストアする場合は、必ず２つ以下の引数を設定してください"
    echo "※第一引数には必ず自身のプラグイン名を設定してください"
fi


####################################################
# 変数の設定
####################################################
#カレントディレクトリをWP-CONTENTに移動する
cd ${ARG_MY_PLUGIN_NAME}
#WP-CONTENTパスを取得する
WP_CONTENT_DIR=$1/

echo $WP_CONTENT_DIR

#logファイルを作成する
LOG_FILE="backup-process-status.txt"
COMPLETE_FILE="complete.txt"
CURRENT_BACKUP_DIR="${WP_CONTENT_DIR}/"


#ROOT権限デフォルト値設定
ALLOW_ROOT=""
#WP設定画面で設定したスクリプトを読み込む
source ${WP_CONTENT_DIR}backup_addtional_script.sh


#ステータスファイルを生成する
rm -f ${CURRENT_BACKUP_DIR}${LOG_FILE}
touch ${CURRENT_BACKUP_DIR}${LOG_FILE}
chown apache:apache ${CURRENT_BACKUP_DIR}${LOG_FILE}
chmod 777 ${CURRENT_BACKUP_DIR}${LOG_FILE}
#ステータスファイルに出力する（第二引数設定時は作成しない）
if [ $NO_OF_ARGS -eq 1 ]; then
    #echo `date '+%Y%m%d_%H%M%S'`: ROOT権限 = ${ALLOW_ROOT} >> ${CURRENT_BACKUP_DIR}${LOG_FILE}
    echo `date '+%Y%m%d_%H%M%S'`: スクリプト実行環境のテストを開始しました >> ${CURRENT_BACKUP_DIR}${LOG_FILE}
    echo スクリプト実行環境のテストを開始しました
fi

#WP-CLIが正常に動作しているかどうか確認する
admin_email=`wp option get admin_email ${ALLOW_ROOT}`    #管理者用メールアドレスを取得する
echo $admin_email
#echo `date '+%Y%m%d_%H%M%S'`": エラー判定 admin_email：　${admin_email} ${#admin_email}"
if [ ${#admin_email} -eq 0 ]; then
#logファイルを作成する
echo `date '+%Y%m%d_%H%M%S'`": スクリプト実行環境に問題があります" > ${WP_CONTENT_DIR}${LOG_FILE}
echo "スクリプト実行環境に問題があります<br />設定を確認してください" > ${WP_CONTENT_DIR}${COMPLETE_FILE}

echo スクリプト実行環境に問題があります
exit 1
fi


#タイムスタンプを取得、変数に設定する
SYSDATE=`date '+%Y%m%d_%H%M%S_'`

#完了ファイルを生成する
echo ${SYSDATE}: スクリプト実行環境に問題ありませんでした
echo "スクリプト実行環境に問題ありませんでした" > ${CURRENT_BACKUP_DIR}${LOG_FILE}

if [ "$ARG_AUTOEXEC_MODE" != "1" ]; then
    echo "スクリプト実行環境が確認されました！" > ${CURRENT_BACKUP_DIR}${COMPLETE_FILE}
fi
exit 0
