<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
if ( ! function_exists( 'neve_child_load_css' ) ):
	/**
	 * Load CSS file.
	 */
	function neve_child_load_css() {
		wp_enqueue_style( 'neve-child-style', trailingslashit( get_stylesheet_directory_uri() ) . 'style.css', array( 'neve-style' ), NEVE_VERSION );
	}
endif;
add_action( 'wp_enqueue_scripts', 'neve_child_load_css', 20 );

    //カスタムフィールドを追加する
    add_action( 'admin_menu', 'add_custom_field' );
    //POST_TYPE: shop_orderに新規カスタムフィールドを追加する
    function add_custom_field() {
      add_meta_box( 'custom-shop_order-trackingid', 'Ttracking ID', 'create_costum_text_field', 'shop_order', 'normal','default', array( 'key' => 'shop_order_trackingid', 'notes' => 'This is a tracking ID for a delivery company'));
    }

    // カスタムフィールドのHTMLを追加する時の処理
    // 第二引数にはadd_meta_boxに設定したオプションデータが渡される
    function create_costum_text_field($post, $metabox) {
      $keyname = $metabox['args']['key'];
      $notes = @$metabox['args']['notes'];
      //global $post;
      // 保存されているカスタムフィールドの値を取得
      $get_value = get_post_meta( $post->ID, $keyname, true );
      // nonceの追加
      wp_nonce_field( 'action-' . $keyname, 'nonce-' . $keyname );
      // HTMLの出力
      echo '<input style="color: black;background-color: lightyellow" name="' . $keyname . '" value=\'' . $get_value . '\'>';
      if(isset($notes) and $notes !== ""){
        echo '<div style="color: #ff0000;">'.$notes.'</div>';
      }
    }

    //POST_TYPE: shop_order カスタムフィールドの保存処理を追加する
    add_action( 'save_post', 'save_custom_field' );
    function save_custom_field( $post_id ) {
      $custom_fields = [
        'shop_order_trackingid',
        ];

      foreach( $custom_fields as $d ) {
        if ( isset( $_POST['nonce-' . $d] ) && $_POST['nonce-' . $d] ) {
          if( check_admin_referer( 'action-' . $d, 'nonce-' . $d ) ) {

            if( isset( $_POST[$d] ) && $_POST[$d] ) {
              update_post_meta( $post_id, $d, $_POST[$d] );
            } else {
              delete_post_meta( $post_id, $d, get_post_meta( $post_id, $d, true ) );
            }
          }
        }
      }
    }

